# README mrio_toolbox

Python package to handle Multi-Regional Input-Output tables in Python

## Intro

mrio_toolbox is designed to manipulate MRIO table with a high level of flexibility

Check out the example directory for an hands-in demo of the package.
