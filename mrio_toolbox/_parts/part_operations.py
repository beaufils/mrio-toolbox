"""Basic operations on Parts

Because of my inexperience in software development,
I only created this file lately.

I will move the relevant methods from the _Part class to this file
at a later point.
"""

def reformat(part,new_dimensions):
    """
    Reshape a Part to match a new dimensions combination

    Equivalent to a combination of the develop and combine_axes methods.

    This only works for contiguous dimensions in the current Part,
    without overlapping dimensions.
    For example, if the Part has dimensions:
        [["countries"],["sectors"],["sectors"]]
    The following is allowed:
        [["countries","sectors"],["sectors"]]
    The following is not allowed:
        [["countries"],["sectors","sectors"]]
        [["sectors"],["countries","sectors"]]
        [["sectors","countries"],["sectors"]]

    Parameters
    ----------
    dimensions : list of list of str
        Original dimensions of the Part

    Returns
    -------
    data : numpy array
        Reshaped data
    axes : list of Axe instances
        Reshaped axes

    """
    def formatting_iteration(part,new_dimensions):
        if part.get_dimensions() == new_dimensions:
            return part
        for i,dim in enumerate(part.get_dimensions()):
            if dim != new_dimensions[i]:
                part = part.combine_axes(i,i+len(new_dimensions)-1)
                return formatting_iteration(part,new_dimensions)
    developed = part.develop()
    return formatting_iteration(developed,new_dimensions)


