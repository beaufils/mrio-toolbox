import os
import re

def check_path(path):
    """
    Extend the name path to avoid overwriting existing files.

    Parameters
    ----------
    path : str
        Path currently selected
    """

    i = 1
    while os.path.isfile(path):
        base_path, ext = os.path.splitext(path)
        
        # Remove existing _number suffix if present
        base_path = re.sub(r'_\d+$', '', base_path)

        path = f"{base_path}_{i}{ext}"
        i += 1
        
    return path