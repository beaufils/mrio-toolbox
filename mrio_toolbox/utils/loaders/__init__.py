from mrio_toolbox.utils.loaders._loader_factory import make_loader

__all__ = ["make_loader"]