"""
Extractor for GLORIA data.

This extractor loads GLORIA raw data files and converts them to NetCDF
files.
  
Supports GLORIA version 059
https://ielab.info/labs/ielab-gloria 

Created on Fr Dez 20, 2024
@author: wirth, based on code of beaufils

"""

import sys
import os
import logging
import numpy as np
import pandas as pd
from mrio_toolbox import MRIO, Part 
from mrio_toolbox.utils.savers._to_nc import save_to_nc

log = logging.getLogger(__name__) #: logger

def extract_gloria(
    year, 
    source, 
    destination,
    markup = 1,
    parts = "basic"):
    """
    Extract GLORIA data. 

    Loads GLORIA tables and labels and store them as NetCDF for further use with 
    the mrio_toolbox library. Currrently, this extractor supports loading T, Y, 
    VA, Q, and QY tables. 
    
    Put all tables (including emission satellite accounts) as well as the 
    'GLORIA_ReadMe_059a.xlsx' file in the same source folder. 
    

    Parameters
    ----------
    year : str
        Data year to load.
    parts : str
        Data blocks to load:
            basic : T, FD
            all : T, FD, VA, QT, QY
    markup : int
        Version of prices to load. Available versions: 
            1 : basic prices
            2 : trade margins
            3 : transport margins
            4 : taxes on products
            5 : subsidies on products
    source : path-like
        Path to folder where raw data is stored
    destination : path-like
        path to folder where NetCDF file will be saved
    
    """

    #Check source path
    if not os.path.exists(source):
        log.error(f"{os.path.abspath(source)} does not exist.")
        raise NotADirectoryError(f"{os.path.abspath(source)} does not exist.")
    
    #Check destination path
    if not os.path.exists(destination):
        log.info(f"{os.path.abspath(
            destination)} does not exist. Creating directory.")
        os.makedirs(destination)

    # Gloria comes with 164 regions (160 countries + rest of americas, 
    # rest of europe, rest of africa, rest of asia-pacific) and 120 sectors. 

    # Usually, we want to extract all tables 
    if parts == "all":
        parts = ["T","Y","V","TQ","YQ"]
    else:
        parts = ["T"]
    
    tables = {}

    # First load the T matrix
    tables["T"] = np.loadtxt(os.path.join(source, f'20240111_120secMother_AllCountries_002_T-Results_{year}_059_Markup00{markup}(full).csv'), 
                         dtype=np.uint32, delimiter=',')
    log.info("physical size of T matrix: %s", sys.getsizeof(tables["T"]))


    '''
    # This code would need very much memory. 
    for part in parts:
        tables[part] = np.loadtxt(
            os.path.join(source,f'20240111_120secMother_AllCountries_002_{part}-Results_{year}_059_Markup00{markup}(full).csv'),
            delimiter = ',')
        log.info(f"Loaded {part} table")
    '''
    
    # Next, we load the labels
    labels = {} 

    countries = pd.read_excel(
        io = os.path.join(source, "GLORIA_ReadMe_059a.xlsx"),
        sheet_name = "Regions")
    
    countries = countries["Region_acronyms"].tolist()

    sectors = pd.read_excel(
        io = os.path.join(source, "GLORIA_ReadMe_059a.xlsx"),
        sheet_name = "Sectors")
    sectors = sectors["Sector_names"].tolist()

    va_and_fd_labs = pd.read_excel(
        io = os.path.join(source, "GLORIA_ReadMe_059a.xlsx"),
        sheet_name = "Value added and final demand")
    fd_labs = va_and_fd_labs["Value_added_names"].tolist()
    va_labs = va_and_fd_labs["Final_demand_names"].tolist()  

    q_labs = pd.read_excel(
        io = os.path.join(source, "GLORIA_ReadMe_059a.xlsx"),
        sheet_name = "Satellites")
    q_labs["combined"] = q_labs["Sat_head_indicator"] + " - " + q_labs["Sat_indicator"] + " - " + q_labs["Sat_unit"]
    q_labs = q_labs["combined"].tolist()


    labels["countries"] = countries
    labels["sectors"] = sectors
    labels["FD_labels"] = fd_labs
    labels["Q_labels"] = q_labs
    labels["VA_labels"] = va_labs

    
    # build an MRIO object from labels and tables
    m = MRIO()
    m.add_dimensions(labels)
    m.parts["T"] = m.new_part(name="T",
        data= tables["T"],
        dimensions = [["countries","sectors"],["countries", "sectors"]])
    m.parts["FD"] = m.new_part(name="FD",
        data= tables["FD"],
        dimensions = [["countries","sectors"],["countries", "FD_labels"]])
    m.parts["VA"] = m.new_part(name="VA",
        data= tables["VA"],
        dimensions = ["VA_labels",["countries","sectors"]])
    m.parts["Q"] = m.new_part(name="Q",
        data= tables["Q"],
        dimensions = ["Q_labels",["countries","sectors"]])
    m.parts["QY"] = m.new_part(name="QY",
        data= tables["QY"],
        dimensions = ["Q_labels",["countries","FD_labels"]])    

    # Last, save the mrio object to a NetCDF file
    save_to_nc(m, destination)
    log.info("Finished converting raw gloria data to NetCDF")

extract_gloria(year=2017,
             source='/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/GLORIA release 059/GLORIA_MRIOs_59_2017',
             destination="/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/GLORIA release 059/GLORIA_MRIOs_59_2017")
