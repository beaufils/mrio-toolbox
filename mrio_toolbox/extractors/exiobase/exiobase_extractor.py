# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 10:43:49 2023

Load and convert Exiobase Industries MRIO files.

Supports Exiobase 3.9.5 in csv https://zenodo.org/records/14869924 

This is the supporting information excel sheet:
https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjiec.12715&file=jiec12715-sup-0009-SuppMat-9.xlsx

@author: wirth
"""

import os
import sys
import numpy as np
import pandas as pd
import logging

# Add your local directory to sys.path
local_lib_path = os.path.abspath("/home/florian/job_merkator_institut/MRIO_intercomparison/mrio-toolbox")
sys.path.insert(0, local_lib_path)

from mrio_toolbox import MRIO, Part 
from mrio_toolbox.utils.savers._to_nc import save_to_nc

s,c = 163,49
log = logging.getLogger(__name__) #: logger
logging.basicConfig(level=logging.INFO)

def extract_exiobase(
    year, 
    source, 
    destination,
    satellites = 'all'):
    
    """
    Load and preformat an EXIOBASE 3 table.

    Parameters
    ----------
    year : str
        Data year to load.
    source : path-like
        Path to folder where raw data is stored
    destination : path-like
        path to folder where NetCDF file will be saved
    satellites : str
        Satellite accounts to load:
            basic : air emissions
            all : air_emissions, employment, energy, land, material, nutrients, water
    path : path-like
        Path to raw data.

    Returns
    -------
    tables : dict of {str : 2D numpy array}
        Keys correspond to matrix parts.
        Values are the numerical tables stored as numpy arrays.
    labels : dict of {str : list of str}
        Dictionnary of countries and sectors labels.

    """
    #Check source path
    if not os.path.exists(source):
        log.error(f"{os.path.abspath(source)} does not exist.")
        raise NotADirectoryError(f"{os.path.abspath(source)} does not exist.")

    #Check destination path
    if not os.path.exists(destination):
        log.info(f"{os.path.abspath(
        destination)} does not exist. Creating directory.")
        os.makedirs(destination)
        
    # EXIOBASE 3 comes with: 
    # - 43 countries + 5 ROW regions 
    # - 163 industries
    # - 9 final demand categories
    # - 9 factor input categories (including value added and taxes)
    # - various satellite accounts
     #c,s,fd,fi = 48,163, 5, 9,


    parts = ["T", "Y", "FI", "FIY","Q_air", "QY_air"]
    
    # Load labels
    log.info("Loading labels...")
    country_labs = pd.read_csv(os.path.join(source,"unit.txt"), delimiter="\t")
    country_labs = country_labs["region"].tolist()
    seen = set()  # Remove duplicates while preserving order
    country_labs = [x for x in country_labs if not (x in seen or seen.add(x))]
    sector_labs = pd.read_csv(os.path.join(source,"unit.txt"), delimiter="\t")
    sector_labs = sector_labs[sector_labs["region"] == "AT"]["sector"].tolist()
    fd_labs = pd.read_csv(os.path.join(source,"Y.txt"), header=1, dtype= "str", delimiter="\t")
    fd_labs = fd_labs.columns[2:9]
    fi_labs = pd.read_csv(os.path.join(source, "factor_inputs", "unit.txt"), dtype= "str", delimiter="\t")
    fi_labs = fi_labs.iloc[:,0].tolist()
    air_emission_labs = pd.read_csv(os.path.join(source, "air_emissions", "unit.txt"), dtype= "str", delimiter="\t")
    air_emission_labs = air_emission_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
    
    labels = {
            "country_labs" : country_labs,
            "sector_labs" : sector_labs, 
            "fd_labs" : fd_labs,
            "fi_labs" : fi_labs,
            "air_emission_labs" : air_emission_labs,
            }
    
    if satellites == 'all':
        
        parts.extend(["Q_employment", "QY_employment", "Q_energy", "QY_energy", "Q_land", "QY_land","Q_material", "QY_material", "Q_nutrients", "QY_nutrients", "Q_water", "QY_water"])
        
        employment_labs  = pd.read_csv(os.path.join(source, "employment", "unit.txt"), dtype= "str", delimiter="\t")
        labels["employment_labs"] = employment_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
        energy_labs  = pd.read_csv(os.path.join(source, "energy", "unit.txt"), dtype= "str", delimiter="\t")
        labels["energy_labs"] = energy_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
        land_labs  = pd.read_csv(os.path.join(source, "land", "unit.txt"), dtype= "str", delimiter="\t")
        labels["land_labs"] = land_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
        material_labs  = pd.read_csv(os.path.join(source, "material", "unit.txt"), dtype= "str", delimiter="\t")
        labels["material_labs"] = material_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
        nutrient_labs  = pd.read_csv(os.path.join(source, "nutrients", "unit.txt"), dtype= "str", delimiter="\t")
        labels["nutrient_labs"] = nutrient_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
        water_labs  = pd.read_csv(os.path.join(source, "water", "unit.txt"), dtype= "str", delimiter="\t")
        labels["water_labs"] = water_labs.apply(lambda row: f"{row.iloc[0]} - {row.iloc[1]}", axis=1).tolist()
    
    log.info("Labels loaded")
        
    # Load tables

    tables = {}
    log.info("Loading IO tables, this can take a while...")
    for part in parts: 
        if part == "T": 
             tables[part] = pd.read_csv(os.path.join(source, "Z.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 2:].to_numpy().astype(float)
             log.info(f"Loaded {part}")
        elif part == "Y":
            tables[part] = pd.read_csv(os.path.join(source, "Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 2:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "FI":
            tables[part] = pd.read_csv(os.path.join(source, "factor_inputs", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "FIY":
            tables[part] = pd.read_csv(os.path.join(source, "factor_inputs", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_air":
            tables[part] = pd.read_csv(os.path.join(source, "air_emissions", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_air":
            tables[part] = pd.read_csv(os.path.join(source, "air_emissions", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_employment":
            tables[part] = pd.read_csv(os.path.join(source, "employment", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_employment":
            tables[part] = pd.read_csv(os.path.join(source, "employment", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_energy":
            tables[part] = pd.read_csv(os.path.join(source, "energy", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_energy":
            tables[part] = pd.read_csv(os.path.join(source, "energy", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_land":
            tables[part] = pd.read_csv(os.path.join(source, "land", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_land":
            tables[part] = pd.read_csv(os.path.join(source, "land", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_material":
            tables[part] = pd.read_csv(os.path.join(source, "material", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_material":
            tables[part] = pd.read_csv(os.path.join(source, "material", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_nutrients":
            tables[part] = pd.read_csv(os.path.join(source, "nutrients", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_nutrients":
            tables[part] = pd.read_csv(os.path.join(source, "nutrients", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "Q_water":
            tables[part] = pd.read_csv(os.path.join(source, "water", "F.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        elif part == "QY_water":
            tables[part] = pd.read_csv(os.path.join(source, "water", "F_Y.txt"), delimiter = "\t", dtype = "str", header = None).iloc[3:, 1:].to_numpy().astype(float)
            log.info(f"Loaded {part}")
        else:
            tables[part] = None
            log.info(f"Didn't load {part}")
        
    log.info("Tables loaded")
    m = MRIO()
    m.add_dimensions(labels)
    
    log.info("Building MRIO objects from parts containing labels and tables...")
    m.parts["T"] = m.new_part(name="T",
        data= tables["T"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "sector_labs"]])
    log.info("T part added")
    m.parts["Y"] = m.new_part(name="Y",
        data= tables["Y"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "fd_labs"]])
    log.info("Y part added")
    m.parts["FI"] = m.new_part(name="FI",
        data= tables["FI"],
        dimensions = ["fi_labs",["country_labs","sector_labs"]])
    log.info("FI part added")
    m.parts["FIY"] = m.new_part(name="FIY",
        data= tables["FIY"],
        dimensions = ["fi_labs",["country_labs","fd_labs"]])
    log.info("FIY part added")
    m.parts["Q_air"] = m.new_part(name="Q_air",
        data = tables["Q_air"],
        dimensions = ["air_emission_labs",["country_labs","sector_labs"]])
    log.info("Q_air part added")
    m.parts["QY_air"] = m.new_part(name="QY_air",
        data = tables["QY_air"],
        dimensions = ["air_emission_labs",["country_labs","fd_labs"]])
    log.info("QY_air part added")
    
    if(satellites == 'all'): 
        m.parts["Q_employment"] = m.new_part(name="Q_employment",
        data = tables["Q_employment"],
        dimensions = ["employment_labs",["country_labs","sector_labs"]])
        log.info("Q_employment part added")
        
        m.parts["QY_employment"] = m.new_part(name="QY_employment",
            data = tables["QY_employment"],
            dimensions = ["employment_labs",["country_labs","fd_labs"]])
        log.info("QY_employment part added")
        
        m.parts["Q_energy"] = m.new_part(name="Q_energy",
        data = tables["Q_energy"],
        dimensions = ["energy_labs",["country_labs","sector_labs"]])
        log.info("Q_energy part added")
        
        m.parts["QY_energy"] = m.new_part(name="QY_energy",
            data = tables["QY_energy"],
            dimensions = ["energy_labs",["country_labs","fd_labs"]])
        log.info("QY_energy part added")
        
        m.parts["Q_land"] = m.new_part(name="Q_land",
        data = tables["Q_land"],
        dimensions = ["land_labs",["country_labs","sector_labs"]])
        log.info("Q_land part added")
        
        m.parts["QY_land"] = m.new_part(name="QY_land",
            data = tables["QY_land"],
            dimensions = ["land_labs",["country_labs","fd_labs"]])
        log.info("QY_land part added")
        
        m.parts["Q_material"] = m.new_part(name="Q_material",
        data = tables["Q_material"],
        dimensions = ["material_labs",["country_labs","sector_labs"]])
        log.info("Q_material part added")
        
        m.parts["QY_material"] = m.new_part(name="QY_material",
            data = tables["QY_material"],
            dimensions = ["material_labs",["country_labs","fd_labs"]])
        log.info("QY_material part added")
        
        m.parts["Q_nutrients"] = m.new_part(name="Q_nutrients",
        data = tables["Q_nutrients"],
        dimensions = ["nutrient_labs",["country_labs","sector_labs"]])
        log.info("Q_nutrients part added")
        
        m.parts["QY_nutrients"] = m.new_part(name="QY_nutrients",
            data = tables["QY_nutrients"],
            dimensions = ["nutrient_labs",["country_labs","fd_labs"]])
        log.info("QY_nutrients part added")
        
        m.parts["Q_water"] = m.new_part(name="Q_water",
        data = tables["Q_water"],
        dimensions = ["water_labs",["country_labs","sector_labs"]])
        log.info("Q_water part added")
        
        m.parts["QY_water"] = m.new_part(name="QY_water",
            data = tables["QY_water"],
            dimensions = ["water_labs",["country_labs","fd_labs"]])
        log.info("QY_water part added")
        
    # Last, save the mrio object to a NetCDF file
    destination = os.path.join(destination, f"EXIOBASE3_{year}_IOT_ixi_{satellites}_satellites.nc")
    save_to_nc(m, destination, overwrite=False)
    log.info("Saved Exiobase data to NetCDF")
                              
extract_exiobase(year=2004, 
                 source = "/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/EXIOBASE 3/release 3.9.5/IOT_2004_ixi", 
                 destination= "/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/EXIOBASE 3/release 3.9.5/IOT_2004_ixi")