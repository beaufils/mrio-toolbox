"""
Load and convert Figaro MRIO files. 

Supports Figaro 2024 in csv matrix format
https://ec.europa.eu/eurostat/web/esa-supply-use-input-tables/database#Input-output%20tables%20industry%20by%20industry

@author: wirth
"""

import os
import sys
import logging
import numpy as np
import pandas as pd

# Add your local directory to sys.path
local_lib_path = os.path.abspath("/home/florian/job_merkator_institut/MRIO_intercomparison/mrio-toolbox")
sys.path.insert(0, local_lib_path)

from mrio_toolbox import MRIO, Part 
from mrio_toolbox.utils.savers._to_nc import save_to_nc

log = logging.getLogger(__name__) #: logger
logging.basicConfig(level=logging.INFO)

def extract_figaro(year, source, destination):
    """
    Extract FIGARO data. 

    Loads FIGARO tables and labels and store them as NetCDF for further use with 
    the mrio_toolbox library. Currently the extractor does not support emission 
    satellite accounts (I couldn't find them on the figaro website).
    
    Put all tables  as well as the 'Description_FIGARO_Tables(24ed).xlsx' file 
    in the same source folder. 

    Parameters
    ----------
    year : str
        Data year to load.
    source : path-like
        Path to folder where raw data is stored
    destination : path-like
        path to folder where NetCDF file will be saved
    """
    
    log.info(f"Extracting FIGARO data for year {year}, load IO table...")
    raw = pd.read_csv(os.path.join(source, f"matrix_eu-ic-io_ind-by-ind_24ed_{year}.csv"), dtype = str)
    log.info("Loaded IO table")
    
    log.info("Extracting labels from raw data...")
    column_labs = raw.columns[1:]
    country_labs = column_labs.str.split("_").str[0]
    country_labs = list(dict.fromkeys(country_labs))
    sector_and_y_labs = column_labs.str.split("_", n=1).str[1]
    sector_and_y_labs = list(dict.fromkeys(sector_and_y_labs))
    sector_labs = sector_and_y_labs[:-5]
    y_labs = sector_and_y_labs[-5:]
    row_labs = raw.rowLabels
    row_labs = row_labs.str.split("_", n=1).str[1]
    row_labs = list(dict.fromkeys(row_labs))
    fi_labs = row_labs[-6:]

    labels_codes = {
        "country_labs": country_labs,
        "sector_labs": sector_labs,
        "y_labs": y_labs,
        "fi_labs": fi_labs # fi = factor income which includes both taxes and value added
    }
    
    # convert label codes into verbose labels which are read in from the description excel file
    df = pd.read_excel(os.path.join(source, "Description_FIGARO_Tables(24ed).xlsx"), header=5, sheet_name = "Prod, Ind & Accounting items")
    df_code = df[["Code.1", "Code.2", "Code.3"]].melt(value_name="code", var_name="code_column")
    df_label = df[["Label.1", "Label.2", "Label.3"]].melt(value_name="label", var_name="label_column")
    label_description = pd.concat([df_code["code"], df_label["label"]], axis=1).reset_index(drop=True)
    label_description = label_description.dropna()
    labels = {}
    for keys in labels_codes: 
        labels[keys] = []
        if keys != "country_labs":
            for code in labels_codes[keys]:  
                labels[keys].append(label_description.loc[label_description["code"] == code, "label"].values[0])
    labels["country_labs"] = labels_codes["country_labs"]
    log.info("Extracted labels from raw data")
    
    log.info("Extracting parts from raw data...")
    raw = raw.iloc[:, 1:].astype(float).to_numpy()
    c, s, y, fi = len(country_labs), len(sector_labs), len(y_labs), len(fi_labs)
    tables = {}
    tables["T"] = raw[:c*s, :c*s]
    tables["Y"] = raw[:c*s, c*s:c*s + c*y]
    tables["FI"] = raw[c*s:c*s+c*fi, : c*s]
    tables["FIY"] = raw[c*s:c*s+c*fi, c*s: c*s + c*y]
    log.info("Extracted parts from raw data")

    # Assemble mrio object
    log.info("Building MRIO object...")
    m = MRIO()
    m.add_dimensions(labels)
    log.info("Building MRIO objects from parts containing labels and tables...")
    m.parts["T"] = m.new_part(name="T",
        data= tables["T"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "sector_labs"]])
    log.info("T part added")
    m.parts["Y"] = m.new_part(name="Y",
        data= tables["Y"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "y_labs"]])
    log.info("Y part added")
    m.parts["FI"] = m.new_part(name="FI",
        data= tables["FI"],
        dimensions = ["fi_labs",["country_labs", "sector_labs"]])
    log.info("FI part added")
    m.parts["FIY"] = m.new_part(name="FIY",
        data= tables["FIY"],
        dimensions = ["fi_labs",["country_labs", "y_labs"]])
    log.info("FIY part added")
    log.info("MRIO object built")

    log.info("Saving MRIO object to NetCDF...")
    destination = os.path.join(destination, f"FIGARO_{year}.nc")
    save_to_nc(m, destination, overwrite=False)
    log.info("Saved Exiobase data to NetCDF")
    
extract_figaro(year=2011, 
    source = "/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/FIGARO", 
    destination= "/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/FIGARO")
