# -*- coding: utf-8 -*-
"""
Created on Wed May 11 16:15:09 2022
Major modifications on 04.02.2025 by wirth

Load and convert ICIO MRIO files. 
Please put the Readme excel file in the same folder as the data and adjust the filename in the code.
Potentially, you have to adjust the sheet names of the excel file, if the OECD changes them. 

Supports ICIO 2021 in csv format
https://www.oecd.org/sti/ind/inter-country-input-output-tables.htm

@author: beaufils and wirth
"""

import os
import sys
import logging
import numpy as np
import pandas as pd

# Add your local directory to sys.path
local_lib_path = os.path.abspath("/home/florian/job_merkator_institut/MRIO_intercomparison/mrio-toolbox")
sys.path.insert(0, local_lib_path)

from mrio_toolbox import MRIO, Part 
from mrio_toolbox.utils.savers._to_nc import save_to_nc

log = logging.getLogger(__name__) #: logger

readme_filename = 'ReadMe_ICIO_extended.xlsx'

def extract_and_save_icio(year, source, destination):
    tables, labels = extract(year = year, parts='all', path = source)
    build_and_save_mrio(tables, labels, destination, year)
    
def build_and_save_mrio(tables, labels, destination, year): 
    m = MRIO()   
    m.add_dimensions(labels)
    
    m.parts["T"] = m.new_part(name="T",
        data= tables["T"],
        dimensions = [["countries","sectors"],["countries", "sectors"]])
    m.parts["FD"] = m.new_part(name="FD",
        data= tables["FD"],
        dimensions = [["countries","sectors"],["countries_fd", "fd_labs"]])
    m.parts["TAX"] = m.new_part(name="TAX",
        data= tables["TAX"],
        dimensions = ["tax_labs",["countries","sectors"]])
    m.parts["TAX_FD"] = m.new_part(name="TAX_FD",
        data= tables["TAX_FD"],
        dimensions = ["tax_labs",["countries_fd","fd_labs"]])
    m.parts["VA"] = m.new_part(name="VA",
        data= tables["VA"],
        dimensions = ["va_labs",["countries", "sectors"]])

    # Last, save the mrio object to a NetCDF file
    destination = os.path.join(destination, f"ICIO_{year}_extended.nc")
    save_to_nc(m, destination, overwrite=False)
    
    #log.info("Finished converting raw wiod data to NetCDF")
    
def extract(year, parts, path):
    """
    Loads and preformat the 2021 release of the ICIO tables.
    
    Follows the syntax imposed by the mrio_formatter.py script.

    Parameters
    ----------
    year : str
        Data year to load.
    parts : str
        Data blocks to load:
            basic : T, FD
            all : T, FD, VA, TAX
    path : path-like
        Path to raw data.
        
    Returns
    -------
    tables : dict of 2D numpy array
        Numerical tables.
    labels : dict of {str : list of str}
        Dictionnary of countries and sectors labels.

    """
    tables = dict()
    
    sectors = load_sectors(path)
    countries = load_countries(path)
    countries_fd = countries[:-4] # remove MX1, MX2, CN1, CN2
    fd_labs = load_fd_labs(path)
    va_labs = ["Taxes less subsidies on intermediate and final products"]
    tax_labs = ["Taxes on products"]
    s,c = len(sectors),len(countries)
    
    raw = load_raw_ICIO(path,year,2021)
     
    if parts == "all":
        parts = ["T","FD","VA","TAX","TAX_FD"]
    else:
        parts = ["T", "FD"]
    
    for part in parts:
        if part == "T":
            tables[part] = raw[:c*s,:c*s]
        elif part == "FD":
            tables[part] = raw[:c*s,c*s:-1]
        elif part == "VA":
            tables[part] = raw[-2,:c*s].reshape((1,c*s))
        elif part == "TAX":
            tables[part] = raw[-3,:c*s].reshape((1,c*s))
        elif part == "TAX_FD":
            tables[part] = raw[-3,c*s:-1].reshape((1,-1))
        else:
            log.warning(f"Part {part} is not part of the ICIO structure"+\
                  "and is ignored.")
            continue
    
    labels = {
        "countries" : countries,
        "countries_fd" : countries_fd,
        "sectors" : sectors,
        "fd_labs" : fd_labs,
        "va_labs" : va_labs,
        "tax_labs" : tax_labs,
        }
    
    return tables, labels

def load_countries(path):
    """
    Load the country list

    Parameters
    ----------
    path : path-like
        Path to the folder storing the raw data.

    Returns
    -------
    countries : list of str
        List of countries covered.
    """
    
    source = pd.read_excel(os.path.join(
        path, readme_filename),
        sheet_name='Area_Activities',
        header=2)['countries'].to_numpy(dtype=str)
    countries = source[source!='nan']
   
    return countries.tolist()

def load_sectors(path):
    """
    Load the list of sectors from the documentation file.

    Parameters
    ----------
    path : path-like
        Path to the folder storing the raw data.

    Returns
    -------
    sectors : list of str
        List of named sectors in the database.

    """
    source = pd.read_excel(os.path.join(
        path, readme_filename),
        sheet_name='Area_Activities',
        header=2)['Industry'].to_numpy(dtype=str)
    sectors = source[source!='nan']
    
    return sectors.tolist()

def load_fd_labs(path):
    """
    Load the list of final demand labels from the documentation file.

    Parameters
    ----------
    path : path-like
        Path to the folder storing the raw data.

    Returns
    -------
    fd_labs : list of str
        List of named final demand labels in the database.

    """
    source = pd.read_excel(os.path.join(
        path, readme_filename),
        sheet_name='ColItems',
        header=3)
    
    index = source[source["Sector code"] == "Final demand items"].index[0]
    source = source.iloc[index:,4].to_numpy(dtype=str)
    
    return source.tolist()

def load_raw_ICIO(path,year,release=2021):
    """
    Load the raw ICIO table

    Parameters
    ----------
    path : path-like
        Path to the folder storing the raw data.
    year : int
        Year to load
    release : int, optional
        Year of release of the data.
        This variable is used to build the prefix to load the table.

    Returns
    -------
    numpy array
        Full ICIO table as 2D numpy array.

    """
    return pd.read_csv(os.path.join(path, f'{year}.csv'),
                  header=0,index_col=0).to_numpy()

extract_and_save_icio(year = 2016,
                      source= '/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/OECD ICIO/2016-2020_extended',
                      destination='/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/OECD ICIO/2016-2020_extended')
