"""
Extractor for WIOD 2016 files. 

This script extracts data from WIOD xlsb files and converts them to NetCDF files 
for further use with the MRIO toolbox. 

Supports WIOD 2016 in Excel format
https://www.rug.nl/ggdc/valuechain/wiod/wiod-2016-release


Created on 08.01, 2024
@author: wirth, based on code of beaufils
"""

import os
import logging
import numpy as np
import pandas as pd
from mrio_toolbox import MRIO, Part 
from mrio_toolbox.utils.savers._to_nc import save_to_nc

log = logging.getLogger(__name__) #: logger

def extract_wiod(
    year, 
    release,
    source, 
    destination,
    parts = 'all'):
    """
    Extract WIOD data. 

    Loads WIOD tables and labels and store them as NetCDF for further use with 
    the mrio_toolbox library. 

    Parameters
    ----------
    year : str
        Data year to load.
    parts : str
        Data blocks to load:
            basic : T, FD
            all : T, FD, VA, QT, QY
    source : path-like
        Path to folder where raw data is stored
    destination : path-like
        path to folder where NetCDF file will be saved
    
    """

        
    #Check source path
    if not os.path.exists(source):
        log.error(f"{os.path.abspath(source)} does not exist.")
        raise NotADirectoryError(f"{os.path.abspath(source)} does not exist.")

    #Check destination path
    if not os.path.exists(destination):
        log.info(f"{os.path.abspath(
        destination)} does not exist. Creating directory.")
        os.makedirs(destination)

    # WIOD 2016 comes with: 
    # - 43 countries + ROW 
    # - 56 sectors 
    # - 5 final demand categories
    # - 1 value added category
    # - 5 tax categories
    c,s,fd,tx = 44,56,5,5
    
    log.info("Start loading")
    
    tables = {}
    raw = load_raw_WIOD(source, year)
    country_labs, sector_labs, fd_labs, tax_labs, va_labs, = [],[],[],[],[]
    labels = raw.columns
    for i in range(c):
        country_labs.append(labels[i*s][2])
    for i in range(s):
        sector_labs.append(labels[i][1])
    for i in range(fd):
        fd_labs.append(labels[s*c + i][1])
    for i in range(tx-1):
        tax_labs.append(raw.index[s*c + 1 + i][1])
    tax_labs.append(raw.index[-2][1])
    va_labs = raw.index[-3][1]
    va_labs = [va_labs]
    
    raw = raw.to_numpy()
    if parts == "all":
        parts = ["T","FD","VA","TAX", "TAX_FD"]
    else:
        parts = ["T", "FD"]

    for part in parts:
        if part == "T":
            tables[part] = raw[:c*s,:c*s]
        elif part == "FD":
            tables[part] = raw[:c*s,c*s:-1]
        elif part == "VA":
            tables[part] = raw[-3,:c*s].reshape(1, -1)
        elif part == "TAX":
            tables[part] = raw[c*s+1:c*s+5,:c*s]
            transport_margins = raw[-2,:c*s].reshape(1, -1)
            tables[part] = np.append(tables[part],transport_margins, axis=0)
        elif part == "TAX_FD":
            tables[part] = raw[c*s+1:c*s+5,c*s:-1]
            transport_margins = raw[-2,c*s:-1].reshape(1, -1)
            tables[part] = np.append(tables[part],transport_margins, axis=0)
        else:
            tables[part] = None

    # build an MRIO object from labels and tables        
    m = MRIO()   
    labels = {
        "country_labs" : country_labs,
        "sector_labs" : sector_labs, 
        "fd_labs" : fd_labs,
        "va_labs" : va_labs,
        "tax_labs" : tax_labs,
        }
    m.add_dimensions(labels)
    
    m.parts["T"] = m.new_part(name="T",
        data= tables["T"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "sector_labs"]])
    m.parts["FD"] = m.new_part(name="FD",
        data= tables["FD"],
        dimensions = [["country_labs","sector_labs"],["country_labs", "fd_labs"]])
    m.parts["TAX"] = m.new_part(name="TAX",
        data= tables["TAX"],
        dimensions = ["tax_labs",["country_labs","sector_labs"]])
    m.parts["TAX_FD"] = m.new_part(name="TAX_FD",
        data= tables["TAX_FD"],
        dimensions = ["tax_labs",["country_labs","fd_labs"]])
    m.parts["VA"] = m.new_part(name="VA",
        data= tables["VA"],
        dimensions = ["va_labs",["country_labs", "sector_labs"]])

    # Last, save the mrio object to a NetCDF file
    destination = os.path.join(destination, f"WIOD{year}_Nov_{release}_ROW.nc")
    save_to_nc(m, destination, overwrite=False)
    log.info("Finished converting raw wiod data to NetCDF")


def load_raw_WIOD(path, year,release=16):
    """
    Load the raw WIOD matrix

    Parameters
    ----------
    year : int-like
    release : int-like, optional
        Version of the WIOD database. The default is 2016.

    Returns
    -------
    Pandas DataFrame
        Full WIOD table as pandas DataFrame.

    """
    return pd.read_excel(os.path.join(path, 
                                      f'WIOT{year}_Nov{release}_ROW.xlsb'),
                  header=[2,3,4,5],index_col=[0,1,2,3])

extract_wiod(year=2007,
             release=16,
             source='/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs/WIOD 2016 release',
             destination="/home/florian/job_merkator_institut/MRIO_intercomparison/MRIOs")
