mrio_toolbox.utils.converters.xarray
====================================

.. py:module:: mrio_toolbox.utils.converters.xarray

.. autoapi-nested-parse::

   Routines for converting between xarray DataArrays and Parts objects.

   ..
       !! processed by numpydoc !!


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.converters.xarray.to_DataArray
   mrio_toolbox.utils.converters.xarray.to_DataSet
   mrio_toolbox.utils.converters.xarray.make_part
   mrio_toolbox.utils.converters.xarray.make_mrio


Module Contents
---------------

.. py:function:: to_DataArray(part)

   
   Convert a Part object to an xarray DataArray

   Labels are directly passed to the DataArray as coords.




   :Returns:

       xr.DataArray
           Corresponding DataArray











   ..
       !! processed by numpydoc !!

.. py:function:: to_DataSet(mrio)

.. py:function:: make_part(data, **kwargs)

   
   Load a Part object from an xarray DataArray


   :Parameters:

       **data** : DataArray
           Part object to load

       **name** : str, optional
           Name of the data variable to load, by default None.
           This can be left empty if there's a single variable in the DataArray.



   :Returns:

       dict
           Data required to create the Part object











   ..
       !! processed by numpydoc !!

.. py:function:: make_mrio(data, **kwargs)

   
   Load an MRIO object from an xarray DataSet


   :Parameters:

       **data** : DataArray
           Part object to load



   :Returns:

       dict
           Data required to create the Part object











   ..
       !! processed by numpydoc !!

