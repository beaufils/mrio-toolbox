mrio_toolbox.utils.converters.pandas
====================================

.. py:module:: mrio_toolbox.utils.converters.pandas

.. autoapi-nested-parse::

   Routines for converting between Pandas DataFrames and Parts objects.

   ..
       !! processed by numpydoc !!


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.converters.pandas.to_pandas
   mrio_toolbox.utils.converters.pandas.make_part
   mrio_toolbox.utils.converters.pandas.autodecode_labels
   mrio_toolbox.utils.converters.pandas.convert_labels
   mrio_toolbox.utils.converters.pandas.disambiguate_labels


Module Contents
---------------

.. py:function:: to_pandas(part)

   
   Return the current Part object as a Pandas DataFrame

   Only applicable to Parts objects with 1 or 2 dimensions.















   ..
       !! processed by numpydoc !!

.. py:function:: make_part(df, name='from_df', label_detection=False, **kwargs)

   
   Load a Part object from a Pandas DataFrame


   :Parameters:

       **df** : DataFrame
           DataFrame to load

       **label_detection** : bool, optional
           Automatically detect labels, by default False
           If True, the DataFrame is scanned to detect labels (defined as non-numeric data)

       **name** : str, optional
           Name of the data variable to load, by default None.
           This can be left empty if there's a single variable in the DataFrame.



   :Returns:

       dict
           Data required to create the Part object











   ..
       !! processed by numpydoc !!

.. py:function:: autodecode_labels(df)

   
   Automatically detect the labels from a DataFrame

   This is done by indentifying the indices and columns
   with non-numeric values.















   ..
       !! processed by numpydoc !!

.. py:function:: convert_labels(index)

   
   Convert a Pandas Index to a dictionary of labels


   :Parameters:

       **index** : Index
           Pandas Index to convert














   ..
       !! processed by numpydoc !!

.. py:function:: disambiguate_labels(labels)

   
   Disambiguate the labels

   This allow solving labels ambiguity if the name was incorrectly loaded.

   :Parameters:

       **index** : dict of str:list of str
           New index to disambiguate

       **labels** : list of str:list of str
           List of labels to disambiguate














   ..
       !! processed by numpydoc !!

