mrio_toolbox.utils.loaders._loader_factory
==========================================

.. py:module:: mrio_toolbox.utils.loaders._loader_factory


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._loader_factory.log


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.loaders._loader_factory.make_loader
   mrio_toolbox.utils.loaders._loader_factory.load_from_yaml


Module Contents
---------------

.. py:data:: log

.. py:function:: make_loader(**kwargs)

   
   Initialize the appropriate loader based on the provided parameters.

   If a file or data_file is provided, 
   the function will attempt to determine the appropriate loader based on the file extension.

   Namely:
   - .nc files are loaded using the NetCDF_Loader
   - .yaml files are interpreted as loading instructions

   All non-netCDF files are loaded using the Parameter_Loader.















   ..
       !! processed by numpydoc !!

.. py:function:: load_from_yaml(**kwargs)

   
   Create a loader based on yaml file instructions.


   :Parameters:

       **file** : path-like
           Full path to the .yaml file














   ..
       !! processed by numpydoc !!

