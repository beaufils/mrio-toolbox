mrio_toolbox.utils.loaders._parameter_loader
============================================

.. py:module:: mrio_toolbox.utils.loaders._parameter_loader

.. autoapi-nested-parse::

   Routine for loading MRIO tables from explicit parameters

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._parameter_loader.log


Classes
-------

.. autoapisummary::

   mrio_toolbox.utils.loaders._parameter_loader.Parameter_Loader


Module Contents
---------------

.. py:data:: log

.. py:class:: Parameter_Loader(**kwargs)

   Bases: :py:obj:`mrio_toolbox.utils.loaders._loader.Loader`


   
   Class for loading MRIO data from explicit parameters.
















   ..
       !! processed by numpydoc !!

   .. py:attribute:: labels


   .. py:attribute:: extension


   .. py:attribute:: part_settings


   .. py:method:: available_parts(extension=None)

      
      List the available parts in the current path.


      :Parameters:

          **extension** : str, optional
              Extension of the files to look for.
              If not provided, all files are listed.



      :Returns:

          list
              List of available parts











      ..
          !! processed by numpydoc !!


   .. py:method:: extract_path(update=False, **kwargs)

      
      Extract the path from the kwargs.

      Valid formats are:
      - path
      - mrio_path
      - file
      - data_path
      - table/year/version
      In absence of explicit path, the current directory is used.

      :Parameters:

          **update** : bool, optional
              Whether to update the path attribute.
              If a path is already set, it is not overridden.














      ..
          !! processed by numpydoc !!


   .. py:method:: format_labels(labels)

      
      Treat the label information

      If labels are provided as dict, they are kept as is.
      If labels are provided as string, they are loaded from the labels_path folder.
      The labels are stored as a dict of lists.















      ..
          !! processed by numpydoc !!


   .. py:method:: load_mrio(**kwargs)

      
      Load MRIO data from explicit parameters.

      If parameters are provided, they overload the corresponding instance attributes.















      ..
          !! processed by numpydoc !!


   .. py:method:: get_file(**kwargs)

      
      Get the file to load.


      :Parameters:

          **file** : path-like, optional
              User-defined path to the file, by default None



      :Returns:

          path-like
              Path to the file to load from




      :Raises:

          ValueError
              If no file is provided nor currently loaded







      ..
          !! processed by numpydoc !!


   .. py:method:: load_part(**kwargs)

      
      Load a Part from explicit parameters.

      Parameters provided as arguments overload the corresponding instance attributes.




      :Returns:

          dict
              Data for creating the Part object




      :Raises:

          FileNotFoundError
              If no file nor name argument is provided







      ..
          !! processed by numpydoc !!


   .. py:method:: _get_labels(l)

      
      Find the labels fitting an axis with a given shape

      If no fitting label is found, data are labelled numerically

      :Parameters:

          **l** : int, list or str
              Length of the data dimension or name of the dimensions.



      :Returns:

          dict of str:list of str
              Labels of the axis.











      ..
          !! processed by numpydoc !!


