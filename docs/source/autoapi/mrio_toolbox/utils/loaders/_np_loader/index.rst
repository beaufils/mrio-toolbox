mrio_toolbox.utils.loaders._np_loader
=====================================

.. py:module:: mrio_toolbox.utils.loaders._np_loader

.. autoapi-nested-parse::

   Routine for loading MRIO Parts from .npy and .csv files

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._np_loader.log


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.loaders._np_loader.load_file
   mrio_toolbox.utils.loaders._np_loader.load_yaml
   mrio_toolbox.utils.loaders._np_loader.load_npy
   mrio_toolbox.utils.loaders._np_loader.load_csv
   mrio_toolbox.utils.loaders._np_loader.load_txt
   mrio_toolbox.utils.loaders._np_loader.load_xlsx


Module Contents
---------------

.. py:data:: log

.. py:function:: load_file(file, extension=None, pandas=False, **kwargs)

   
   Load data from a .npy, .txt, .xlsx or .csv file.


   :Parameters:

       **file** : path-like
           Full path to the file

       **kwargs** : dict
           Additional parameters for the loaders



   :Returns:

       **data** : np.array
           Numerical data




   :Raises:

       FileNotFoundError
           If the file is not found in the specified path

       ValueError
           If the file extension is not supported







   ..
       !! processed by numpydoc !!

.. py:function:: load_yaml(file, **kwargs)

.. py:function:: load_npy(file, **kwargs)

.. py:function:: load_csv(file, pandas=False, **kwargs)

   
   Read a .csv file using pandas or numpy.

   If pandas, the file is read using pandas,
   such that labels are automatically extracted.
   Otherwise, the file is read using numpy and labels are loaded from another file.















   ..
       !! processed by numpydoc !!

.. py:function:: load_txt(file, **kwargs)

.. py:function:: load_xlsx(file, **kwargs)

