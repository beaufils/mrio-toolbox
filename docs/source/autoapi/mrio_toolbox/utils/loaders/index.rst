mrio_toolbox.utils.loaders
==========================

.. py:module:: mrio_toolbox.utils.loaders


Submodules
----------

.. toctree::
   :maxdepth: 1

   /autoapi/mrio_toolbox/utils/loaders/_loader/index
   /autoapi/mrio_toolbox/utils/loaders/_loader_factory/index
   /autoapi/mrio_toolbox/utils/loaders/_nc_loader/index
   /autoapi/mrio_toolbox/utils/loaders/_np_loader/index
   /autoapi/mrio_toolbox/utils/loaders/_pandas_loader/index
   /autoapi/mrio_toolbox/utils/loaders/_parameter_loader/index


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.loaders.make_loader


Package Contents
----------------

.. py:function:: make_loader(**kwargs)

   
   Initialize the appropriate loader based on the provided parameters.

   If a file or data_file is provided, 
   the function will attempt to determine the appropriate loader based on the file extension.

   Namely:
   - .nc files are loaded using the NetCDF_Loader
   - .yaml files are interpreted as loading instructions

   All non-netCDF files are loaded using the Parameter_Loader.















   ..
       !! processed by numpydoc !!

