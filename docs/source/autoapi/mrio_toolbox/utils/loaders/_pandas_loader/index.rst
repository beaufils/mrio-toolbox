mrio_toolbox.utils.loaders._pandas_loader
=========================================

.. py:module:: mrio_toolbox.utils.loaders._pandas_loader

.. autoapi-nested-parse::

   Routines for loading from Excel

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._pandas_loader.log


Classes
-------

.. autoapisummary::

   mrio_toolbox.utils.loaders._pandas_loader.Pandas_Loader


Module Contents
---------------

.. py:data:: log

.. py:class:: Pandas_Loader(**kwargs)

   Bases: :py:obj:`mrio_toolbox.utils.loaders._parameter_loader.Parameter_Loader`


   
   Class for loading MRIO data through Pandas.
















   ..
       !! processed by numpydoc !!

   .. py:method:: load_part(**kwargs)

      
      Load a Part from explicit parameters.

      Parameters provided as arguments overload the corresponding instance attributes.




      :Returns:

          dict
              Data for creating the Part object




      :Raises:

          FileNotFoundError
              If no file nor name argument is provided







      ..
          !! processed by numpydoc !!


