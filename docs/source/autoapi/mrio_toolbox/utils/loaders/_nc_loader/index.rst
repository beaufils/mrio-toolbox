mrio_toolbox.utils.loaders._nc_loader
=====================================

.. py:module:: mrio_toolbox.utils.loaders._nc_loader


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._nc_loader.log


Classes
-------

.. autoapisummary::

   mrio_toolbox.utils.loaders._nc_loader.NetCDF_Loader


Module Contents
---------------

.. py:data:: log

.. py:class:: NetCDF_Loader(**kwargs)

   Bases: :py:obj:`mrio_toolbox.utils.loaders._loader.Loader`


   
   Class for loading MRIO data from a netCDF file.
















   ..
       !! processed by numpydoc !!

   .. py:method:: load_mrio(file=None, **kwargs)

      
      Load a netcdf file in the memory.

      This procedure is based on the xarray library.
      The xarray dataset is stored in the data attribute.
      The loader also extracts all metadata from the file.

      :Parameters:

          **file** : path-like, optional
              Full path to the file.
              If left empty, the file currently initialised is used.







      :Raises:

          ValueError
              If the file is not provided.







      ..
          !! processed by numpydoc !!


   .. py:method:: load_part(file=None, **kwargs)

      
      Load a part of the MRIO table.


      :Parameters:

          **name** : str
              Name of the variable to load

          **file** : path, optional
              Full path to the data.
              If left empty, the current xarray Dataset is used.



      :Returns:

          dict
              Data required to create a Part object











      ..
          !! processed by numpydoc !!


   .. py:method:: get_file(file=None, **kwargs)

      
      Get the file to load.


      :Parameters:

          **file** : path-like, optional
              User-defined path to the file, by default None



      :Returns:

          path-like
              Path to the file to load from




      :Raises:

          ValueError
              If no file is provided nor currently loaded







      ..
          !! processed by numpydoc !!


   .. py:method:: available_parts(**kwargs)

      
      Return a list of available parts in the MRIO table.





      :Returns:

          list
              List of available parts











      ..
          !! processed by numpydoc !!


