mrio_toolbox.utils.savers
=========================

.. py:module:: mrio_toolbox.utils.savers


Submodules
----------

.. toctree::
   :maxdepth: 1

   /autoapi/mrio_toolbox/utils/savers/_path_checker/index
   /autoapi/mrio_toolbox/utils/savers/_to_folder/index
   /autoapi/mrio_toolbox/utils/savers/_to_nc/index


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.savers.save_mrio_to_folder
   mrio_toolbox.utils.savers.save_part_to_folder
   mrio_toolbox.utils.savers.save_to_nc


Package Contents
----------------

.. py:function:: save_mrio_to_folder(obj, path, name=None, extension='.npy', overwrite=False, **kwargs)

   
   Save an MRIO instance in a folder


   :Parameters:

       **path** : str
           Path to the folder to save the MRIO instance into.

       **extension** : str, optional
           Extension of the files to save the MRIO instance into.
           The default is "npy".

       **overwrite** : bool, optional
           Whether to overwrite the existing files. The default is False.
           If False, the version name is iterated until a non-existing
           file name is found.

       **kwargs** : dict
           Additional arguments to pass to the saver.














   ..
       !! processed by numpydoc !!

.. py:function:: save_part_to_folder(obj, path, name=None, extension='.npy', save_labels=True, write_instructions=True, overwrite=False, include_labels=True, **kwargs)

   
   Save a Part instance in a folder


   :Parameters:

       **obj** : Part
           Part instance to save

       **path** : str
           Path to the folder to save the Part instance into.

       **extension** : str, optional
           Extension of the files to save the Part instance into.
           The default is ".npy".

       **save_labels** : bool, optional
           Whether to save the labels. The default is True.

       **save_instructions** : bool, optional
           Whether to save the instructions. The default is True.

       **overwrite** : bool, optional
           Whether to overwrite the existing files. The default is False.
           If False, the version name is iterated until a non-existing
           file name is found.

       **include_labels: bool, optional**
           Whether to include the labels in the file. The default is True.
           This is only relevant for .csv and .xlsx files.
           If False, the labels are saved in a separate file.

       **kwargs** : dict
           Additional arguments to pass to the saver.














   ..
       !! processed by numpydoc !!

.. py:function:: save_to_nc(obj, path, overwrite=False, write_instructions=False, **kwargs)

   
   Save an MRIO or Path instance in a .nc file


   :Parameters:

       **path** : str
           Path to the .nc file to save the MRIO instance into.

       **\*\*kwargs** : dict
           Additional arguments to pass to the saver.














   ..
       !! processed by numpydoc !!

