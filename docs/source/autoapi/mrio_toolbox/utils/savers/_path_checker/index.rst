mrio_toolbox.utils.savers._path_checker
=======================================

.. py:module:: mrio_toolbox.utils.savers._path_checker


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.savers._path_checker.check_path


Module Contents
---------------

.. py:function:: check_path(path)

   
   Extend the name path to avoid overwriting existing files.


   :Parameters:

       **path** : str
           Path currently selected














   ..
       !! processed by numpydoc !!

