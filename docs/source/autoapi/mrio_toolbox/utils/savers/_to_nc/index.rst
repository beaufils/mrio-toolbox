mrio_toolbox.utils.savers._to_nc
================================

.. py:module:: mrio_toolbox.utils.savers._to_nc


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.savers._to_nc.log


Functions
---------

.. autoapisummary::

   mrio_toolbox.utils.savers._to_nc.save_to_nc


Module Contents
---------------

.. py:data:: log

.. py:function:: save_to_nc(obj, path, overwrite=False, write_instructions=False, **kwargs)

   
   Save an MRIO or Path instance in a .nc file


   :Parameters:

       **path** : str
           Path to the .nc file to save the MRIO instance into.

       **\*\*kwargs** : dict
           Additional arguments to pass to the saver.














   ..
       !! processed by numpydoc !!

