mrio_toolbox
============

.. py:module:: mrio_toolbox


Submodules
----------

.. toctree::
   :maxdepth: 1

   /autoapi/mrio_toolbox/_parts/index
   /autoapi/mrio_toolbox/mrio/index
   /autoapi/mrio_toolbox/utils/index


Classes
-------

.. autoapisummary::

   mrio_toolbox.MRIO
   mrio_toolbox.Part


Functions
---------

.. autoapisummary::

   mrio_toolbox.load_part


Package Contents
----------------

.. py:class:: MRIO(**kwargs)

   
   Representation of an MRIO table

   An MRIO table holds a collection of Parts, each representing a different
   aspect of the table (inter-industry matrix, final demand, etc.)
   The MRIO instance allows to perform basic operations on the table.



   .. rubric:: Methods



   ==============================  ==========
       **load_parts(parts,year)**  Load new parts  
             **load_zones(name)**  load new zones  
       **rename_zone(zone,name)**  rename a zone  
                **modify_zone()**    
   **load_extension(extension):**  Load a new MRIO extension.  
                   **group_y():**  Aggregated final demand by zone.  
     **extract_zone(part,zone):**  Extract a given zone from a given part of the MRIO table.  
                  **sel(index):**  Returns the slice containing the indices of all sectors of a country.  
   ==============================  ==========












   ..
       !! processed by numpydoc !!

   .. py:attribute:: loader


   .. py:attribute:: metadata


   .. py:attribute:: labels


   .. py:attribute:: groupings


   .. py:attribute:: parts


   .. py:method:: load_part(update_part=True, standalone=False, **kwargs)

      
      Load a Part object into the MRIO table

      By default, the Part is loaded using the current loader.

      :Parameters:

          **update_part** : bool, optional
              The groupings and labels of the Part are updated based on the MRIO attributes.

          **standalone** : bool, optional
              Whether to load the Part as a standalone object.
              The default is False.

          **kwargs** : dict
              Additional arguments to pass to the Part loader.














      ..
          !! processed by numpydoc !!


   .. py:method:: _update_labels(part, update_part=True)

      
      Update the labels of the MRIO table with the labels of a Part object

      If all the labels of the Part are already in the MRIO labels,
      the method does nothing.
      This method is run after adding a new Part to the MRIO table.

      :Parameters:

          **part** : Part object
              Part object to use for the update.

          **update_part** : bool, optional
              If True and the Part labels are not properly set,
              tries to update the Part labels based on the MRIO labels.














      ..
          !! processed by numpydoc !!


   .. py:method:: _get_labels(l)

      
      Find the labels fitting an axis with a given shape

      Available labels:
          countries and sectors
          countries
          zones and sectors
          zones
          sectors

      If no fitting label is found, data are labelled numerically

      :Parameters:

          **l** : int
              Length of the data dimension.



      :Returns:

          dict of str:list of str
              Labels of the axis.











      ..
          !! processed by numpydoc !!


   .. py:method:: set_groupings(groupings=None)

      
      Set the groupings of the MRIO table

      Groupings are used to group labels into larger categories 
      (e.g countries into zones, sectors in aggregate sectors).
      Groupings have in principle no impact on the resolution of the table
      but can be used for visualization or aggregation purposes.

      Groupings should be disjoint, but this is not enforced.
      Nested groupings are not supported.

      Unspecified groupings are set to the identity.
      Calling the method without arguments resets the groupings to the identity.

      :Parameters:

          **groupings** : dict of dict, optional
              Groupings of the MRIO table.
              The default is None.
              If None, the groupings are set to the identity.
              Groupings should be provided as a dict of dict:
                  {dimension : {group : [items]}}
              where dimension is the name of the label to group,
              group is the name of the group,
              and items is a list of items to group.














      ..
          !! processed by numpydoc !!


   .. py:method:: _update_groupings()

      
      Update the groupings of all Parts of the MRIO instance
















      ..
          !! processed by numpydoc !!


   .. py:method:: filter(threshold, fill_value=0)

      
      Filter the MRIO table by removing values below a threshold


      :Parameters:

          **threshold** : float
              Value below which the values are set to 0.

          **fill_value** : float, optional
              Value to use to fill the table if only dimensions are given.



      :Returns:

          None.
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: add_part(part, name=None, update_part=True)

      
      Add a Part object to the MRIO table


      :Parameters:

          **part** : Part object
              Part object to add to the MRIO table.

          **update_part** : bool, optional
              Whether to update the labels of the Part object.
              The default is True.














      ..
          !! processed by numpydoc !!


   .. py:method:: new_part(data=None, name='part', dimensions=None, fill_value=0.0, **kwargs)

      
      Cast part data into the corresponding Part Object


      :Parameters:

          **data** : np.ndarray, optional
              Data to load in the Part. The default is None.
              If None, the dimensions argument is used to create an empty Part.

          **name** : str, optional
              Name of the Part. The default is "part".

          **dimensions or labels** : list of str, list of ints, str, list of dicts, optional
              Labels of the Part. 
              Either of these formats are accepted:
                  Dictionary of explicit labels for each axis
                  List of explicit labels for each axis
                  List of existing dimension names
              If None, the labels are inferred from the data shape.

          **multiplier** : str, optional
              multiplier of the data. The default is None.

          **unit** : float, optional
              Unit of the data. The default is 1.

          **fill_value** : float, optional
              Value to use to fill the table if only dimensions are given.



      :Returns:

          Part instance
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: add_dimensions(dimensions)

      
      Add dimensions to the MRIO table


      :Parameters:

          **dimensions** : dict
              Description of the dimension to add.














      ..
          !! processed by numpydoc !!


   .. py:method:: add_labels(new_indices, dimension, fill_value=0.0)

      
      Add items to a label of the MRIO instance

      All Parts are updated automatically with the given fill_value

      :Parameters:

          **new_indices** : list of str
              items to add to the label

          **dimension** : str
              name of the labels to which the new indices should be added

          **fill_value** : float, optional
              Value to use to fill the newly created label fields in the tables.














      ..
          !! processed by numpydoc !!


   .. py:method:: replace_labels(name, new_labels)

      
      Replace labels in all MRIO parts


      :Parameters:

          **names** : str
              Name of the labels 

          **new_labels** : dict of str:list of str
              Description of the new labels to use.














      ..
          !! processed by numpydoc !!


   .. py:method:: rename_labels(old_names, new_names)

      
      Rename labels for the MRIO instance


      :Parameters:

          **old_names** : str or list of str
              Labels to rename.

          **new_names** : str or list of str
              New names for the labels.














      ..
          !! processed by numpydoc !!


   .. py:method:: aggregate(on='sectors')

      
      Aggregate the MRIO table on a given dimension

      The aggregation is performed by summing the values of the table
      for the items that are grouped together.

      :Parameters:

          **on** : str, optional
              Name of the dimension to aggregate on. 
              The default is "sectors".



      :Returns:

          None.
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: __getattr__(name)


   .. py:method:: __setattr__(name, value)


   .. py:method:: __str__()


   .. py:method:: has_neg(parts=None)

      
      Check whether some Parts have negative values


      :Parameters:

          **parts** : str, list of str or None, optional
              List of parts to inspect. 
              If left empty, all parts are inspected.



      :Returns:

          bool
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: copy()

      
      Create a copy of the MRIO object
















      ..
          !! processed by numpydoc !!


   .. py:method:: save(file, name=None, extension='npy', overwrite=False, **kwargs)

      
      Save the current MRIO instance

      If the path points to a folder, the MRIO parts can be saved as:
          - .npy
          - .csv
          - .txt
          - .xlsx
      Labels are saved as .txt files and metadata as a .yaml file.

      Otherwise the MRIO instance is saved as a .nc file.

      :Parameters:

          **file** : str
              Full path to the file or folder to save the MRIO instance into.
              If a file is provided, the extension is used to determine the format.

          **extension** : str, optional
              Extension of the file to save the MRIO instance into.
              This is only used if the file is a folder.

          **overwrite** : bool, optional
              Whether to overwrite the existing file. The default is False.
              If False, the version name is iterated until a non-existing
              file name is found.

          **kwargs** : dict
              Additional arguments to pass to the saver.














      ..
          !! processed by numpydoc !!


   .. py:method:: to_xarray()

      
      Convert the MRIO instance to an xarray Dataset
















      ..
          !! processed by numpydoc !!


.. py:class:: Part(data=None, labels=None, axes=None, **kwargs)

   .. py:attribute:: name


   .. py:attribute:: groupings


   .. py:attribute:: metadata


   .. py:method:: alias(**kwargs)

      
      Create a new Part in which only prescribed parameters are changed

      The current Part is taken as reference: all arguments not explicitely
      set are copied from the current part.















      ..
          !! processed by numpydoc !!


   .. py:method:: _create_axis(labels)

      
      Create an Axe object based on a tuple of lists of indices


      :Parameters:

          **\*args** : tuple of lists of str, list of str
              Labels of the axe.
              The first argument is used as the main label.
              The second argument (if any) is used as secondary label.
              If left empty, the axe is labelled by indices only



      :Returns:

          None.
              ..




      :Raises:

          TypeError
              Raised if the arguments types differs from the number of dimensions
              or if input labels are incorrect.

          ValueError
              Raised if the label length does not match the data.







      ..
          !! processed by numpydoc !!


   .. py:method:: fix_dims(skip_labels=False, skip_data=False)

      
      Align the number of axes with the number of dimensions

      If one length exceeds the other, axes and/or data are squeezed,
      i.e. dimensions of length 1 are removed.















      ..
          !! processed by numpydoc !!


   .. py:method:: __getitem__(args)


   .. py:method:: __setitem__(args, value)


   .. py:method:: setter(value, *args)

      
      Change the value of a data selection


      :Parameters:

          **value** : float or numpy like
              Value to set.

          **\*args** : list of tuples
              Indices along the respective axes.



      :Returns:

          None.
              ..

          Modification is applied to the current Part object
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: get(*args, aspart=True, squeeze=False)

      
      Extract data from the current Part object


      :Parameters:

          **\*args** : list of tuples
              Selection along the Axes of the Part.

          **aspart** : bool, optional
              Whether to return the selection as a Part object. 
              If False, the selection is returned as a numpy object.
              The default is True.

          **squeeze** : bool, optional
              Whether to remove dimensions of length 1.
              The default is False



      :Returns:

          New Part object or numpy object
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: develop(axis=None, on=None, squeeze=True)

      
      Reshape a Part to avoid double labels


      :Parameters:

          **axis** : int or list of int, optional
              Axis to develop. 
              If left empty, all axes are developed.
              The default is None.

          **on** : str or list of str, optional
              Dimensions to develop.
              If left empty, all dimensions are developed.
              Note that the develop method does not support the developping of
              non-contiguous dimensions.
              The default is None.

          **squeeze** : bool, optional
              Whether to remove dimensions of length 1.
              The default is True.



      :Returns:

          Part object
              Developped Part











      ..
          !! processed by numpydoc !!


   .. py:method:: reformat(new_dimensions)

      
      Reshape a Part to match a new dimensions combination

      Equivalent to a combination of the develop and combine_axes methods.

      This only works for contiguous dimensions in the current Part,
      without overlapping dimensions.
      For example, if the Part has dimensions:
          [["countries"],["sectors"],["sectors"]]
      The following is allowed:
          [["countries","sectors"],["sectors"]]
      The following is not allowed:
          [["countries"],["sectors","sectors"]]
          [["sectors"],["countries","sectors"]]
          [["sectors","countries"],["sectors"]]

      :Parameters:

          **dimensions** : list of list of str
              Original dimensions of the Part



      :Returns:

          **data** : numpy array
              Reshaped data

          **axes** : list of Axe instances
              Reshaped axes











      ..
          !! processed by numpydoc !!


   .. py:method:: combine_axes(start=0, end=None, in_place=False)

      
      Combine axes of a Part into a single one.

      The order of dimensions is preserved in the new axis.
      Only consecutive axes can be combined.
      The method can be used to revert the develop method.

      :Parameters:

          **start** : int, optional
              Index of the first axis to combine, by default 0

          **end** : int, optional
              Index of the final axis to combine, by default None,
              all axis are combined, i.e. the Part is flattened.



      :Returns:

          Part instance
              ..




      :Raises:

          IndexError
              Axes should have no overlapping dimensions.







      ..
          !! processed by numpydoc !!


   .. py:method:: swap_axes(axis1, axis2)

      
      Swap two axes of a Part


      :Parameters:

          **axis1** : int
              First axis to swap.

          **axis2** : int
              Second axis to swap.



      :Returns:

          Part instance
              Part with swapped axes.











      ..
          !! processed by numpydoc !!


   .. py:method:: swap_ax_levels(axis, dim1, dim2)

      
      Swap two levels of an axis


      :Parameters:

          **axis** : int
              Axis to modify.

          **dim1** : str
              First dimension to swap.

          **dim2** : str
              Second dimension to swap.



      :Returns:

          Part instance
              Part with swapped levels.











      ..
          !! processed by numpydoc !!


   .. py:method:: flatten(invert=False)

      
      Flatten a 2D Part into a 1D Part


      :Parameters:

          **inverse** : bool, optional
              Whether to in the inverse level order.














      ..
          !! processed by numpydoc !!


   .. py:method:: squeeze()


   .. py:method:: expand_dims(axis, copy=None)

      
      Add dimensions to a Part instance


      :Parameters:

          **axis** : int
              Position of the new axis.

          **copy** : int, optional
              Axis to copy the labels from.
              If left empty, the axis is created without labels.














      ..
          !! processed by numpydoc !!


   .. py:method:: copy()

      
      Return a copy of the current Part object
















      ..
          !! processed by numpydoc !!


   .. py:method:: extraction(dimensions, labels=['all'], on_groupings=True, domestic_only=False, axis='all')

      
      Set labels over dimension(s) to 0.


      :Parameters:

          **dimensions** : str, list of str, dict
              Name of the dimensions on which the extraction is done.
              If dict is passed, the keys are interpreted as the dimensions
              and the values as the labels

          **labels** : list of (list of) str, optional
              Selection on the dimension to put to 0.

          **on_groupings** : bool, optional
              Whether to use the groupings to select the labels.
              This matters only when the domestic_only argument is set to True.

          **domestic_only** : bool, optional
              If yes, only domestic transactions are set to 0 and trade flows 
              are left untouched. The default is False.

          **axis** : list of ints, optional
              Axis along which the extraction is done. The default is "all".
              In any case, the extraction only applies to axis allowing it, that
              is in axis containing zones or countries labels corresponding to 
              the zone selection.



      :Returns:

          Part object
              New Part with selection set to 0.











      ..
          !! processed by numpydoc !!


   .. py:method:: leontief_inversion()


   .. py:method:: zone()

      
      Apply a grouping by zone dependent on the shape of the Part.

      Final demand Parts are summed over zones.
      Horizontal extensions are expanded by zone.




      :Returns:

          Part object
              Grouped part.




      :Raises:

          AttributeError
              Parts with other shapes are rejected.







      ..
          !! processed by numpydoc !!


   .. py:method:: update_groupings(groupings, ax=None)

      
      Update the groupings of the current Part object

      groupings : dict
          Description of the groupings
      ax: int, list of int
          Axes to update. If left empty, all axes are updated.















      ..
          !! processed by numpydoc !!


   .. py:method:: aggregate(on='countries', axis=None)

      
      Aggregate dimensions along one or several axis.

      If groupings are defined, these are taken into account.
      If you want to sum over the dimension of an axis, use the sum method.

      If no axis is specified, the operation is applied to all axes.

      :Parameters:

          **axis** : str or list of str, optional
              List of axis along which countries are grouped.
              If left emtpy, countries are grouped along all possible axis.

          **on** : str or dict, optional
              Indicate wether the grouping should be done by zones ("zones")
              or by sector ("sectors"), or both ("both").
              The default is "zones".
              If both, the operation is equivalent to summing over an axis



      :Returns:

          Part object
              Part grouped by zone.




      :Raises:

          ValueError
              Raised if a selected Axe cannot be grouped.







      ..
          !! processed by numpydoc !!


   .. py:method:: aggregate_on(on, axis)

      
      Aggregate a Part along a given axis


      :Parameters:

          **on** : str
              Dimension to aggregate on

          **axis** : int
              Axis to aggregate



      :Returns:

          Part instance
              Aggregated Part











      ..
          !! processed by numpydoc !!


   .. py:method:: get_labels(axis=None)

      
      Returns the dictionnary of the Part labels


      :Parameters:

          **axis** : int or list of int, optional
              Axis to investigate, by default None,
              All axes are investigated.



      :Returns:

          list
              Labels used in the part.











      ..
          !! processed by numpydoc !!


   .. py:method:: list_labels()

      
      List the labels of the Part
















      ..
          !! processed by numpydoc !!


   .. py:method:: get_dimensions(axis=None)

      
      Returns the list dimensions of the Part


      :Parameters:

          **axis** : int or list of int, optional
              Axis to investigate, by default None,
              All axes are investigated.



      :Returns:

          list
              Dimensions of the axes.











      ..
          !! processed by numpydoc !!


   .. py:method:: rename_labels(old, new)

      
      Rename some labels of the Part


      :Parameters:

          **old** : str
              Name of the label to change.

          **new** : str
              New label name.














      ..
          !! processed by numpydoc !!


   .. py:method:: replace_labels(name, labels, axis=None)

      
      Update a label of the part


      :Parameters:

          **name** : str
              Name of the label to update, by default None

          **labels** : dict or list
              New labels for the corresponding ax.
              If a list is passed, the former label name is used.

          **axis** : int, list of int, optional
              List of axis on which the label is changed.
              By default None, all possible axes are updated.














      ..
          !! processed by numpydoc !!


   .. py:method:: set_labels(labels, axis=None)

      
      Change the labels of the Part


      :Parameters:

          **labels** : dict or nested list
              New labels of the axes.
              If a nested list is passed, the first level corresponds to the axes

          **axis** : str, optional
              Axis on which the labels are changes, by default None,
              all axes are updated.














      ..
          !! processed by numpydoc !!


   .. py:method:: _store_labels()

      
      Store the labels of the Part
















      ..
          !! processed by numpydoc !!


   .. py:method:: add_labels(labels, dimension=None, axes=None, fill_value=0)

      
      Add indices to one or multiple Part axes.


      :Parameters:

          **new_labels** : list of str or dict
              List of indices to add

          **dimension** : str, optional
              Labels the new indices should be appended to,
              in case new_labels is not a dict.
              If new_labels is a dict, dimension is ignored.

          **axes** : int or set of ints, optional
              Axes or list of axes to modify.
              In case it is not specified, the axes are detected
              by looking for the dimension (or new_labels keys) in each ax.

          **fill_value** : float, optional
              Value used to initialize the new Part



      :Returns:

          Part instance
              Part instance with the additional ax indices.











      ..
          !! processed by numpydoc !!


   .. py:method:: expand(axis=None, over='countries')

      
      Expand an axis of the Part

      Create a new Axes with a unique dimension.
      Note that this operation significantly expands the size of the Part.
      It is recommended to use this method with Extension parts only.

      :Parameters:

          **axis** : int, optional
              Axe to extend. 
              If left empty, the first suitable axe is expanded.

          **over** : str, optional
              Axe dimension to expand the Part by.
              The default is "countries".



      :Returns:

          Part object
              New Part object with an additional dimension.











      ..
          !! processed by numpydoc !!


   .. py:method:: issquare()

      
      Assert wether the Part is square
















      ..
          !! processed by numpydoc !!


   .. py:method:: hasneg()

      
      Test whether Part has negative elements
















      ..
          !! processed by numpydoc !!


   .. py:method:: hasax(name=None)

      
      Returns the dimensions along which a Part has given labels

      If no axis can be found, an empty list is returned empty.
      This method can be used to assert the existence of a given dimension
      in the part.

      :Parameters:

          **name** : int, optional
              Name of the label to look for.
              If no name is given, all axes are returned.



      :Returns:

          **axes** : list of ints
              Dimensions along which the labels are found.











      ..
          !! processed by numpydoc !!


   .. py:method:: __str__()


   .. py:method:: sum(axis=None, on=None, keepdims=False)

      
      Sum the Part along one or several axis, and/or on a given dimension.


      :Parameters:

          **axis** : int or list of int, optional
              Axe along which the sum is evaluated.
              By default None, the sum of all coefficients of the Part is returned

          **on** : str, optional
              name of the dimension to be summed on.
              If no axis is defined, the Part is summed over all axis having 
              the corresponding dimension.
              By default None, the full ax is summed

          **keepdims** : bool, optional
              Whether to keep the number of dimensions of the original.
              By default False, the dimensions of lenght 1 are removed.



      :Returns:

          Part instance or float
              Result of the sum.











      ..
          !! processed by numpydoc !!


   .. py:method:: _sum_on(axis, on, keepdims=False)

      
      Sum a Part along an axis on a given dimension
















      ..
          !! processed by numpydoc !!


   .. py:method:: save(file=None, name=None, extension='.npy', overwrite=False, include_labels=False, write_instructions=False, **kwargs)

      
      Save the Part object to a file


      :Parameters:

          **name** : str, optional
              Name under which the Part is saved.
              By default, the current part is used.

          **path** : Path-like, optional
              Directory in which the Path should be saved, 
              by default None, the dir from which the part was loaded.

          **extension** : str, optional
              Format under which the part is saved. The default ".npy"
              If ".csv" is chosen, the part is saved as a csv file with labels

          **file** : path-like, optional
              Full path to the file to save the Part to.
              This overrides the path, name and extension arguments.

          **overwrite** : boolm optional
              Whether to overwrite an existing file. 
              If set False, the file is saved with a new name.
              The default is False.

          **write_instructions** : bool, optional
              Whether to write the loading instructions to a yaml file.
              The default is False.

          **include_labels** : bool, optional
              Whether to include the labels in the saved file.
              Only applicable to .csv and .xlsx files.

          **\*\*kwargs** : dict
              Additional arguments to pass to the saving function







      :Raises:

          FileNotFoundError
              _description_







      ..
          !! processed by numpydoc !!


   .. py:method:: to_pandas()

      
      Return the current Part object as a Pandas DataFrame

      Only applicable to Parts objects with 1 or 2 dimensions.















      ..
          !! processed by numpydoc !!


   .. py:method:: to_xarray()

      
      Save the Part object to an xarray DataArray

      Labels are directly passed to the DataArray as coords.
      Note that data will be flattened.
      The dimension order will be saved as an attribute.
      If you're loading the data back,
      the Part will be automatically reshaped to its original dimensions.




      :Returns:

          xr.DataArray
              Corresponding DataArray











      ..
          !! processed by numpydoc !!


   .. py:method:: mean(axis=None)


   .. py:method:: min(axis=None)


   .. py:method:: max(axis=None)


   .. py:method:: mul(a, propagate_labels=True)

      
      Matrix multiplication between parts with labels propagation


      :Parameters:

          **a** : Part or numpy array
              Right-hand multiplicator.

          **propagate_labels** : bool, optional
              Whether to try propagating the labels from the right hand multiplicator
              By default True.
              If right-hand multiplicator is not a Part object, becomes False.



      :Returns:

          Part instance
              result of the multiplication











      ..
          !! processed by numpydoc !!


   .. py:method:: filter(threshold, fill_value=0)

      
      Set to 0 the values below a given threshold


      :Parameters:

          **threshold** : float
              Threshold value.

          **fill_value** : float, optional
              Value to replace the filtered values with.
              The default is 0.



      :Returns:

          Part instance
              Filtered Part.











      ..
          !! processed by numpydoc !!


   .. py:method:: diag()


   .. py:method:: __add__(a)


   .. py:method:: __radd__(a)


   .. py:method:: __rmul__(a)


   .. py:method:: __mul__(a)


   .. py:method:: __neg__()


   .. py:method:: __sub__(a)


   .. py:method:: __rsub__(a)


   .. py:method:: power(a)


   .. py:method:: __pow__(a)


   .. py:method:: __eq__(a)


   .. py:method:: __rtruediv__(a)


   .. py:method:: __truediv__(a)


   .. py:method:: __getattr__(name)


   .. py:method:: transpose()


.. py:function:: load_part(**kwargs)

