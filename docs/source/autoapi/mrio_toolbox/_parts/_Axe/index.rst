mrio_toolbox._parts._Axe
========================

.. py:module:: mrio_toolbox._parts._Axe

.. autoapi-nested-parse::

   Definition of Part Axes

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox._parts._Axe.log


Classes
-------

.. autoapisummary::

   mrio_toolbox._parts._Axe.Axe


Module Contents
---------------

.. py:data:: log

.. py:class:: Axe(labels, groupings=None, name=None)

   .. py:attribute:: levels


   .. py:attribute:: dims


   .. py:attribute:: dimensions


   .. py:attribute:: mappings


   .. py:method:: set_labels(labels)

      
      Set the labels of the Axe


      :Parameters:

          **labels** : dict or list of list of str
              Dict of labels for each level of the Axe.
              The levels are interpreted in the order of the keys.
              If a list of list of str is passed, levels are set to 0,1,2...














      ..
          !! processed by numpydoc !!


   .. py:method:: update_multipliers()

      
      Update the multipliers used to convert labels into indices
















      ..
          !! processed by numpydoc !!


   .. py:method:: squeeze()

      
      Remove levels with only one label
















      ..
          !! processed by numpydoc !!


   .. py:method:: swap_levels(level1, level2)

      
      Swap the positions of two levels in the Axe


      :Parameters:

          **level1** : int
              Level to swap

          **level2** : int
              Level to swap














      ..
          !! processed by numpydoc !!


   .. py:method:: __str__()


   .. py:method:: label(as_index=False)

      
      Generate the labels for the full axis


      :Parameters:

          **as_index** : bool, optional
              Whether to return a Pandas MultiIndex. 
              The default is False, in which case a list of labels is returned.



      :Returns:

          list of str
              Labels along the Axe











      ..
          !! processed by numpydoc !!


   .. py:method:: isin(arg, level)

      
      Assert whether an element is in the labels of a given level


      :Parameters:

          **arg** : str, int or list of int,str
              Element to look for.

          **level** : str
              Key of the level to look into.



      :Returns:

          bool
              Whether the element is in the labels.











      ..
          !! processed by numpydoc !!


   .. py:method:: derive_mappings(groupings=None)

      
      Update the mappings of the Axe

      Mappings are defined at Axe level and derive from the current groupings
      Mappings are used to convert grouping labels into indices

      :Parameters:

          **groupings** : dict of dict
              Dict of groupings for each level of the Axe.
              Grouping of labels into larger categories (e.g countries into zones).
              If groupings contain references not in the labels, these are removed.
              By default, groupings are set to the identity.














      ..
          !! processed by numpydoc !!


   .. py:method:: update_groupings(groupings=None)


   .. py:method:: get_on(arg, level, multiplier)

      
      Recursively get the index of an arg on a given level


      :Parameters:

          **arg** : str, int, list of str, int
              Element to look for.

          **level** : str
              Key of the level to look into.














      ..
          !! processed by numpydoc !!


   .. py:method:: get_single_ax(args)

      
      Make selection for single level Axes
















      ..
          !! processed by numpydoc !!


   .. py:method:: get_labels()

      
      Get the labels of the Axe
















      ..
          !! processed by numpydoc !!


   .. py:method:: get_labs(args)

      
      Extract the labels corresponding to a given selection of multiple levels


      :Parameters:

          **args** : list of ints
              Cleaned selection of indices



      :Returns:

          dict
              Dict of labels for each level











      ..
          !! processed by numpydoc !!


   .. py:method:: get(args, labels=False)

      
      Get the indices corresponding to a selection on the Axe


      :Parameters:

          **args** : str, int, dict, list of str, int
              Arguments to select on the Axe.
              If a dict is passed, it is assumed that the keys are the dimensions of the Axe.
              If a list is passed, it is assumed that the first element is the selection on the first level,
              the second on the second level, etc. 
              If the selection on multipler levels fails, the selection is assumed to be on the first level only.

          **labels** : bool, optional
              Whether to return the labels of the selection, by default False



      :Returns:

          list of int or (list of ints, dict, dict)
              If labels is False, returns the indices of the selection.
              If labels is True, returns the indices of the selection, 
              the labels of the selection and the groupings of the Axe.











      ..
          !! processed by numpydoc !!


   .. py:method:: rename_labels(old, new)

      
      Rename labels in the Axes


      :Parameters:

          **old** : str
              Former label name

          **new** : str
              New name














      ..
          !! processed by numpydoc !!


   .. py:method:: replace_label(name, labels)

      
      Replace a given label


      :Parameters:

          **name** : str
              Name of the level to replace

          **labels** : dict
              New labels














      ..
          !! processed by numpydoc !!


   .. py:method:: __getitem__(*a)

      
      Indexing are passed to the get method
















      ..
          !! processed by numpydoc !!


   .. py:method:: __eq__(other)


   .. py:method:: has_dim(dim)

      
      Check whether a given dimension is in the Axe
















      ..
          !! processed by numpydoc !!


   .. py:method:: __len__()

      
      Get the full label length of the Axe
















      ..
          !! processed by numpydoc !!


