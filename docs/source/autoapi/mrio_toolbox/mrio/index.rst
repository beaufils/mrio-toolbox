mrio_toolbox.mrio
=================

.. py:module:: mrio_toolbox.mrio

.. autoapi-nested-parse::

   Created on Thu Mar 30 10:42:23 2023

   Representation of economic MRIO tables

   @author: beaufils

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox.mrio.log


Classes
-------

.. autoapisummary::

   mrio_toolbox.mrio.MRIO


Module Contents
---------------

.. py:data:: log

.. py:class:: MRIO(**kwargs)

   
   Representation of an MRIO table

   An MRIO table holds a collection of Parts, each representing a different
   aspect of the table (inter-industry matrix, final demand, etc.)
   The MRIO instance allows to perform basic operations on the table.



   .. rubric:: Methods



   ==============================  ==========
       **load_parts(parts,year)**  Load new parts  
             **load_zones(name)**  load new zones  
       **rename_zone(zone,name)**  rename a zone  
                **modify_zone()**    
   **load_extension(extension):**  Load a new MRIO extension.  
                   **group_y():**  Aggregated final demand by zone.  
     **extract_zone(part,zone):**  Extract a given zone from a given part of the MRIO table.  
                  **sel(index):**  Returns the slice containing the indices of all sectors of a country.  
   ==============================  ==========












   ..
       !! processed by numpydoc !!

   .. py:attribute:: loader


   .. py:attribute:: metadata


   .. py:attribute:: labels


   .. py:attribute:: groupings


   .. py:attribute:: parts


   .. py:method:: load_part(update_part=True, standalone=False, **kwargs)

      
      Load a Part object into the MRIO table

      By default, the Part is loaded using the current loader.

      :Parameters:

          **update_part** : bool, optional
              The groupings and labels of the Part are updated based on the MRIO attributes.

          **standalone** : bool, optional
              Whether to load the Part as a standalone object.
              The default is False.

          **kwargs** : dict
              Additional arguments to pass to the Part loader.














      ..
          !! processed by numpydoc !!


   .. py:method:: _update_labels(part, update_part=True)

      
      Update the labels of the MRIO table with the labels of a Part object

      If all the labels of the Part are already in the MRIO labels,
      the method does nothing.
      This method is run after adding a new Part to the MRIO table.

      :Parameters:

          **part** : Part object
              Part object to use for the update.

          **update_part** : bool, optional
              If True and the Part labels are not properly set,
              tries to update the Part labels based on the MRIO labels.














      ..
          !! processed by numpydoc !!


   .. py:method:: _get_labels(l)

      
      Find the labels fitting an axis with a given shape

      Available labels:
          countries and sectors
          countries
          zones and sectors
          zones
          sectors

      If no fitting label is found, data are labelled numerically

      :Parameters:

          **l** : int
              Length of the data dimension.



      :Returns:

          dict of str:list of str
              Labels of the axis.











      ..
          !! processed by numpydoc !!


   .. py:method:: set_groupings(groupings=None)

      
      Set the groupings of the MRIO table

      Groupings are used to group labels into larger categories 
      (e.g countries into zones, sectors in aggregate sectors).
      Groupings have in principle no impact on the resolution of the table
      but can be used for visualization or aggregation purposes.

      Groupings should be disjoint, but this is not enforced.
      Nested groupings are not supported.

      Unspecified groupings are set to the identity.
      Calling the method without arguments resets the groupings to the identity.

      :Parameters:

          **groupings** : dict of dict, optional
              Groupings of the MRIO table.
              The default is None.
              If None, the groupings are set to the identity.
              Groupings should be provided as a dict of dict:
                  {dimension : {group : [items]}}
              where dimension is the name of the label to group,
              group is the name of the group,
              and items is a list of items to group.














      ..
          !! processed by numpydoc !!


   .. py:method:: _update_groupings()

      
      Update the groupings of all Parts of the MRIO instance
















      ..
          !! processed by numpydoc !!


   .. py:method:: filter(threshold, fill_value=0)

      
      Filter the MRIO table by removing values below a threshold


      :Parameters:

          **threshold** : float
              Value below which the values are set to 0.

          **fill_value** : float, optional
              Value to use to fill the table if only dimensions are given.



      :Returns:

          None.
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: add_part(part, name=None, update_part=True)

      
      Add a Part object to the MRIO table


      :Parameters:

          **part** : Part object
              Part object to add to the MRIO table.

          **update_part** : bool, optional
              Whether to update the labels of the Part object.
              The default is True.














      ..
          !! processed by numpydoc !!


   .. py:method:: new_part(data=None, name='part', dimensions=None, fill_value=0.0, **kwargs)

      
      Cast part data into the corresponding Part Object


      :Parameters:

          **data** : np.ndarray, optional
              Data to load in the Part. The default is None.
              If None, the dimensions argument is used to create an empty Part.

          **name** : str, optional
              Name of the Part. The default is "part".

          **dimensions or labels** : list of str, list of ints, str, list of dicts, optional
              Labels of the Part. 
              Either of these formats are accepted:
                  Dictionary of explicit labels for each axis
                  List of explicit labels for each axis
                  List of existing dimension names
              If None, the labels are inferred from the data shape.

          **multiplier** : str, optional
              multiplier of the data. The default is None.

          **unit** : float, optional
              Unit of the data. The default is 1.

          **fill_value** : float, optional
              Value to use to fill the table if only dimensions are given.



      :Returns:

          Part instance
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: add_dimensions(dimensions)

      
      Add dimensions to the MRIO table


      :Parameters:

          **dimensions** : dict
              Description of the dimension to add.














      ..
          !! processed by numpydoc !!


   .. py:method:: add_labels(new_indices, dimension, fill_value=0.0)

      
      Add items to a label of the MRIO instance

      All Parts are updated automatically with the given fill_value

      :Parameters:

          **new_indices** : list of str
              items to add to the label

          **dimension** : str
              name of the labels to which the new indices should be added

          **fill_value** : float, optional
              Value to use to fill the newly created label fields in the tables.














      ..
          !! processed by numpydoc !!


   .. py:method:: replace_labels(name, new_labels)

      
      Replace labels in all MRIO parts


      :Parameters:

          **names** : str
              Name of the labels 

          **new_labels** : dict of str:list of str
              Description of the new labels to use.














      ..
          !! processed by numpydoc !!


   .. py:method:: rename_labels(old_names, new_names)

      
      Rename labels for the MRIO instance


      :Parameters:

          **old_names** : str or list of str
              Labels to rename.

          **new_names** : str or list of str
              New names for the labels.














      ..
          !! processed by numpydoc !!


   .. py:method:: aggregate(on='sectors')

      
      Aggregate the MRIO table on a given dimension

      The aggregation is performed by summing the values of the table
      for the items that are grouped together.

      :Parameters:

          **on** : str, optional
              Name of the dimension to aggregate on. 
              The default is "sectors".



      :Returns:

          None.
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: __getattr__(name)


   .. py:method:: __setattr__(name, value)


   .. py:method:: __str__()


   .. py:method:: has_neg(parts=None)

      
      Check whether some Parts have negative values


      :Parameters:

          **parts** : str, list of str or None, optional
              List of parts to inspect. 
              If left empty, all parts are inspected.



      :Returns:

          bool
              ..











      ..
          !! processed by numpydoc !!


   .. py:method:: copy()

      
      Create a copy of the MRIO object
















      ..
          !! processed by numpydoc !!


   .. py:method:: save(file, name=None, extension='npy', overwrite=False, **kwargs)

      
      Save the current MRIO instance

      If the path points to a folder, the MRIO parts can be saved as:
          - .npy
          - .csv
          - .txt
          - .xlsx
      Labels are saved as .txt files and metadata as a .yaml file.

      Otherwise the MRIO instance is saved as a .nc file.

      :Parameters:

          **file** : str
              Full path to the file or folder to save the MRIO instance into.
              If a file is provided, the extension is used to determine the format.

          **extension** : str, optional
              Extension of the file to save the MRIO instance into.
              This is only used if the file is a folder.

          **overwrite** : bool, optional
              Whether to overwrite the existing file. The default is False.
              If False, the version name is iterated until a non-existing
              file name is found.

          **kwargs** : dict
              Additional arguments to pass to the saver.














      ..
          !! processed by numpydoc !!


   .. py:method:: to_xarray()

      
      Convert the MRIO instance to an xarray Dataset
















      ..
          !! processed by numpydoc !!


