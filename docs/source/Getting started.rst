Getting started
=============== 

Installation
------------

To use the mrio toolbox, install it using pip. It is recommended to use a
virtual environment. 

.. code-block:: console

   (.venv) $ pip install mrio-toolbox

Learning the methods with an example file
-----------------------------------------
Usually, you would want to use the mrio-toolbox to perform input-output analysis
with pre-existing MRIO tables. Therefore you'd need to 
`download raw data <#downloading-raw-data-files>`_ first. 

However, in order to learn the functionings of the library, it is recommended 
to first take a look at the `example notebook`_ file, to learn the basic 
functions. 

.. _example notebook: example.ipynb

 
Downloading raw data files
--------------------------

You need raw data MRIO files in order to use this library. 
There are several place where you could get such files, e.g. 

