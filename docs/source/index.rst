.. mrio-toolbox documentation master file, created by
   sphinx-quickstart on Fri Dec 20 10:42:21 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

mrio-toolbox documentation
==========================

Welcome to the documentation of the MRIO-toolbox, 
a python package to handle multi-regional input-output tables 
with a high level of flexibility. 

Contents:
---------

.. toctree::
   :maxdepth: 2
   
   Getting started
   example.nblink
