# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'mrio-toolbox'
copyright = '2024, Timothé Beaufils'
author = 'Timothé Beaufils'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration


extensions = ['autoapi.extension','numpydoc','nbsphinx', 'nbsphinx_link']
#autoapi_dirs = ['../../extractors' , '../../mrio_toolbox'] # include extractors (has to be cleaned)
autoapi_dirs = ['../../mrio_toolbox']
autoapi_template_dir = "_templates/autoapi" 
autoapi_keep_files = True

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
