mrio_toolbox.utils.loaders._loader
==================================

.. py:module:: mrio_toolbox.utils.loaders._loader

.. autoapi-nested-parse::

   Central loading module for the mrio_toolbox package.

   This module contains the central loading function for the mrio_toolbox package.
   Depending on the loading mode, the function will call the appropriate loader.

   ..
       !! processed by numpydoc !!


Attributes
----------

.. autoapisummary::

   mrio_toolbox.utils.loaders._loader.log


Classes
-------

.. autoapisummary::

   mrio_toolbox.utils.loaders._loader.Loader


Module Contents
---------------

.. py:data:: log

.. py:class:: Loader

   
   Parent class for the loaders
















   ..
       !! processed by numpydoc !!

   .. py:method:: extract_basic_info(**kwargs)

      
      Extract basic information from the loader.

      The function will extract the path, labels and groupings from the loader.















      ..
          !! processed by numpydoc !!


   .. py:method:: update_settings(**settings)

      
      Update the loader settings with new parameters
















      ..
          !! processed by numpydoc !!


   .. py:method:: load_mrio()

      
      Create an MRIO container based on the new parameters





      :Returns:

          dict
              Dictionary of MRIO metadata











      ..
          !! processed by numpydoc !!


   .. py:method:: load_part(**kwargs)

      
      Load an MRIO Part based on new or existing parameters





      :Returns:

          dict
              Dictionary containing the Part data











      ..
          !! processed by numpydoc !!


   .. py:method:: set_groupings(groupings)

      
      Update the groupings attribute of the loader


      :Parameters:

          **groupings** : dict of dict of str
              Aggregation on labels














      ..
          !! processed by numpydoc !!


   .. py:method:: update_attributes(**kwargs)

      
      Update the current attributes of the loader.

      The function will update the groupings, paths, labels and metadata attributes.















      ..
          !! processed by numpydoc !!


   .. py:method:: load_groupings(file, dimension=None, path=None)

      
      Load groupings from a file


      :Parameters:

          **file** : str
              Name of the file to load

          **dimension** : str, optional
              Name of the dimension to load groupings for.
              By default (None), the file is interpreted as a preset
              of groupings on different dimension.

          **path** : path-like, optional
              Path where the file is stored. 
              By default, the groupings are from the settings dir
              in the working dir.














      ..
          !! processed by numpydoc !!


   .. py:method:: set_labels(labels)

      
      Update the labels attribute of the loader


      :Parameters:

          **labels** : dict of str:list of str
              Labels of the axes














      ..
          !! processed by numpydoc !!


   .. py:method:: check_instructions(**kwargs)

      
      Interpret the file argument for loading a part.

      This method solves the ambiguity between data files and optional
      .yaml instructions.
      If the file argument refers to an instruction file, it is compared
      to the current instructions.
      If the data file or instruction file differ from the ones currently loaded,
      an exception is raised to force a reload.

      :Parameters:

          **file** : path-like
              User-provided file path

          **kwargs** : additional arguments
              ..







      :Raises:

          FileNotFoundError
              If the loader needs to be reloaded with new instructions.







      ..
          !! processed by numpydoc !!


