"""
Test the routines for loading and writing data

The tests all follow the same structure:
    1. Random data parts are created
    2. Data parts are saved to a temporary file
    3. Data parts are loaded from the temporary file
    4. The loaded data parts are compared to the original data parts
    5. The temporary file is deleted

The tests are implemented for the combination of the following settings:
    format: 
        - "ncdf"
        - "csv"
        - "xlsx"
        - "npy"
        - "txt"
    loading instructions:
        - explicit
        - through setting file (.yaml)
    loading scale:
        - single part
        - full MRIO
"""


import os
import shutil
import yaml
import unittest
import numpy as np

from mrio_toolbox._parts._Part import Part, load_part
from mrio_toolbox.mrio import MRIO
from test_utils import random_part,random_mrio


class TestBasicPartLoaders(unittest.TestCase):
    def setUp(self):
        self.part = random_part()
        self.base_path = os.path.join("_test","data")
        self.loading_instructions = {
            "path" : self.base_path,
            "name" : "A",
            "labels" : {"rows":
                [
                    "a","b","c"
                ],
                "columns" :
                [
                    "A","B","C"
                ]
            },
            "nature" : "test"
        }
        
    def save_and_load(self, extension,tolerance=False,include_labels=False):
        self.path = os.path.join(self.base_path, "test_"+extension[1:])
        self.part.save(self.path, extension=extension,overwrite=True,include_labels=include_labels)
        self.assertTrue(os.path.exists(self.path + extension))
        loader_kwargs = dict()
        if include_labels:
            loader_kwargs["pandas"] = True
        loaded = load_part(file=self.path+extension,dimensions=["countries","sectors"],
                           loader_kwargs=loader_kwargs)
        if tolerance:
            np.testing.assert_allclose(self.part.data,loaded.data)
            return
        self.assertEqual(self.part, loaded)
        self.assertListEqual(self.part.get_dimensions(),loaded.get_dimensions())

    def test_loading_nc(self):
        self.save_and_load(".nc")

    def test_loading_csv(self):
        self.save_and_load(".csv")

    def test_loading_csv_with_labels(self):
        self.save_and_load(".csv",include_labels=True,tolerance=True)

    def test_loading_xlsx(self):
        #Tolerance is required for xlsx files
        #Because column names are a pain to handle
        self.save_and_load(".xlsx",tolerance=True)

    def test_loading_npy(self):
        self.save_and_load(".npy")

    def test_loading_txt(self):
        self.save_and_load(".txt")

    def test_loading_missing_attributes(self):
        self.assertRaises(FileNotFoundError,MRIO,path="this_file_does_not_exist")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree(self.base_path)
        os.mkdir(self.base_path)

class TestBasicYAMLPartLoaders(unittest.TestCase):
    def setUp(self):
        self.part = random_part()
        self.base_path = os.path.join("_test","data")
        self.loading_instructions = {
            "path" : self.base_path,
            "name" : "A",
            "labels" : {"rows":
                [
                    "a","b","c"
                ],
                "columns" :
                [
                    "A","B","C"
                ]
            },
            "nature" : "test"
        }
        
    def save_and_load(self, extension,tolerance=False):
        self.path = os.path.join(self.base_path, "test_"+extension[1:])
        self.part.save(self.path, extension=extension,overwrite=True,
                       write_instructions=True)
        self.assertTrue(os.path.exists(self.path + extension))
        loaded = load_part(file=self.path+".yaml")
        if tolerance:
            np.testing.assert_allclose(self.part.data,loaded.data)
            return
        self.assertEqual(self.part, loaded)
        self.assertListEqual(self.part.get_dimensions(),loaded.get_dimensions())

    def test_loading_nc(self):
        self.save_and_load(".nc")

    def test_loading_csv(self):
        self.save_and_load(".csv")

    def test_loading_xlsx(self):
        #Tolerance is required for xlsx files
        #Because column names are a pain to handle
        self.save_and_load(".xlsx",tolerance=True)

    def test_loading_npy(self):
        self.save_and_load(".npy")

    def test_loading_txt(self):
        self.save_and_load(".txt")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree(self.base_path)
        os.mkdir(self.base_path)


class TestMultiIndexPartLoaders(unittest.TestCase):
    def setUp(self):
        self.part = random_part(dimensions=[["countries","sectors"],["sectors"]],)
        self.base_path = os.path.join("_test","data")
        self.loading_instructions = {
            "path" : self.base_path,
            "name" : "A",
            "dimensions" : [["countries","sectors"],["sectors"]],
            "nature" : "test"
        }
        
    def save_and_load(self, extension,tolerance=False,include_labels=False):
        self.path = os.path.join(self.base_path, "test_"+extension[1:])
        self.part.save(self.path, extension=extension,overwrite=True,include_labels=include_labels)
        self.assertTrue(os.path.exists(self.path + extension))
        loader_kwargs = dict()
        if include_labels:
            loader_kwargs["pandas"] = True
        loaded = load_part(file=self.path+extension,dimensions=[["countries","sectors"],"sectors"],
                           loader_kwargs=loader_kwargs)
        if tolerance:
            np.testing.assert_allclose(self.part.data,loaded.data,atol=1e-5)    
            return
        self.assertEqual(self.part, loaded)
        self.assertListEqual(self.part.get_dimensions(),loaded.get_dimensions())

    def test_loading_nc(self):
        self.save_and_load(".nc")

    def test_loading_csv(self):
        self.save_and_load(".csv")

    def test_loading_csv_with_labels(self):
        self.save_and_load(".csv",include_labels=True,tolerance=True)

    def test_loading_xlsx(self):
        #Tolerance is required for xlsx files
        #Because column names are a pain to handle
        self.save_and_load(".xlsx",tolerance=True)

    def test_loading_npy(self):
        self.save_and_load(".npy")

    def test_loading_txt(self):
        self.save_and_load(".txt")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree(self.base_path)
        os.mkdir(self.base_path)

class TestSquarePartLoaders(unittest.TestCase):
    def setUp(self):
        self.part = random_part(dimensions=[["sectors"],["sectors"]],)
        self.base_path = os.path.join("_test","data")
        self.loading_instructions = {
            "path" : self.base_path,
            "name" : "A",
            "dimensions" : [["sectors"],["sectors"]],
            "nature" : "test"
        }
        
    def save_and_load(self, extension,tolerance=False,include_labels=False):
        self.path = os.path.join(self.base_path, "test_"+extension[1:])
        self.part.save(self.path, extension=extension,overwrite=True,include_labels=include_labels)
        self.assertTrue(os.path.exists(self.path + extension))
        loader_kwargs = dict()
        if include_labels:
            loader_kwargs["pandas"] = True
        loaded = load_part(file=self.path+extension,dimensions=["sectors","sectors"],
                           loader_kwargs=loader_kwargs)
        if tolerance:
            np.testing.assert_allclose(self.part.data,loaded.data)
            return
        self.assertEqual(self.part, loaded)
        self.assertListEqual(self.part.get_dimensions(),loaded.get_dimensions())

    def test_loading_nc(self):
        self.save_and_load(".nc")

    def test_loading_csv(self):
        self.save_and_load(".csv")

    def test_loading_csv_with_labels(self):
        self.save_and_load(".csv",include_labels=True,tolerance=True)

    def test_loading_xlsx(self):
        #Tolerance is required for xlsx files
        #Because column names are a pain to handle
        self.save_and_load(".xlsx",tolerance=True)

    def test_loading_npy(self):
        self.save_and_load(".npy")

    def test_loading_txt(self):
        self.save_and_load(".txt")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree(self.base_path)
        os.mkdir(self.base_path)

class TestMrioLoaders(unittest.TestCase):
    def setUp(self):
        self.mrio = random_mrio() #Create the MRIO object using random data

    def save_and_load(self, extension,tolerance=False,
                      folder=True,check_labels=False,
                      not_implemented=False):
        self.path = os.path.join("_test","data", "test_"+extension[1:])
        self.mrio.save(self.path, extension=extension,overwrite=True)
        if folder:
            self.assertTrue(os.path.exists(self.path))
            kwargs = {"path":self.path}
        else:
            self.assertTrue(os.path.exists(self.path+extension))
            kwargs = {"file":self.path+extension}
        kwargs["extension"] = extension
        if not_implemented:
            with self.assertRaises(NotImplementedError):
                loaded = MRIO(**kwargs)
            return
        loaded = MRIO(**kwargs)
        for part in loaded.parts:
            if tolerance:
                np.testing.assert_allclose(loaded.parts[part].data,self.mrio.parts[part].data)
                continue
            self.assertEqual(loaded.parts[part],self.mrio.parts[part])
            if check_labels:
                self.assertEqual(
                    loaded.parts[part].get_dimensions(),
                    self.mrio.parts[part].get_dimensions())
            
    def test_loading_nc(self):
        self.save_and_load(".nc",folder=False,
                           check_labels=True)
    
    def test_loading_csv(self):
        self.save_and_load(".csv")
    
    def test_loading_xlsx(self):
        self.save_and_load(".xlsx",tolerance=True)
    
    def test_loading_npy(self):
        self.save_and_load(".npy")
    
    def test_loading_txt(self):
        self.save_and_load(".txt")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree("_test/data")
        os.mkdir("_test/data")

class TestMrioLoadersFromYaml(unittest.TestCase):
    def setUp(self):
        self.mrio = random_mrio() #Create the MRIO object using random data

    def save_and_load(self, extension,tolerance=False,
                      folder=True,check_labels=False,
                      not_implemented=False):
        self.path = os.path.join("_test","data", "test_"+extension[1:])
        self.mrio.save(self.path, extension=extension,overwrite=True)
        if folder:
            self.assertTrue(os.path.exists(self.path))
        else:
            self.assertTrue(os.path.exists(self.path+extension))
        kwargs = {"file":self.path+".yaml"}
        if not_implemented:
            with self.assertRaises(NotImplementedError):
                loaded = MRIO(**kwargs)
            return
        loaded = MRIO(**kwargs)
        for part in loaded.parts:
            if tolerance:
                np.testing.assert_allclose(loaded.parts[part].data,self.mrio.parts[part].data)
                continue
            self.assertEqual(loaded.parts[part],self.mrio.parts[part])
            if check_labels:
                self.assertEqual(
                    loaded.parts[part].get_dimensions(),
                    self.mrio.parts[part].get_dimensions())
            
    def test_loading_nc(self):
        self.save_and_load(".nc",folder=False,
                           check_labels=True)
    
    def test_loading_csv(self):
        self.save_and_load(".csv")
    
    def test_loading_xlsx(self):
        self.save_and_load(".xlsx",tolerance=True)
    
    def test_loading_npy(self):
        self.save_and_load(".npy")
    
    def test_loading_txt(self):
        #Unsupported format without instructions
        #Because of ambiguity with the labels
        self.save_and_load(".txt")

    def tearDown(self):
        #Reset the test folder
        shutil.rmtree("_test/data")
        os.mkdir("_test/data")

class TestAutoMRIOConversions(unittest.TestCase):
    def test_from_DataSet(self):
        mrio = random_mrio()
        ds = mrio.to_xarray()
        loaded = MRIO(data=ds)
        for part in loaded.parts:
            np.testing.assert_allclose(loaded.parts[part].data,mrio.parts[part].data)
            self.assertEqual(
                    loaded.parts[part].get_dimensions(),
                    mrio.parts[part].get_dimensions())
        self.assertEqual(loaded.metadata,mrio.metadata)

    def test_from_DataArray(self):
        part = random_part()
        da = part.to_xarray()
        loaded = MRIO(data=da)
        self.assertEqual(loaded.parts[part.name],part)

    def test_from_df(self):
        part = random_part()
        df = part.to_pandas()
        loaded = MRIO(data=df)
        self.assertEqual(loaded.parts["from_df"],part)