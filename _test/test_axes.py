import unittest
import numpy as np
from mrio_toolbox._parts._Axe import Axe

labels = {
    "countries": ["A","B","C","D","E"],
    "sectors": ["s1","s2","s3"],
}

groupings = {
    "countries": {
        "z1" : ["A","C"],
        "z2" : ["B","D"],
    },
    "sectors" : {
        "g1" : ["s1","s2"],
        "g2" : ["s3"],
    }
}

class Test_Axe_Init(unittest.TestCase):
    def test_basic_init(self):
        a = Axe(labels["countries"])
        self.assertEqual(a.levels,1)
        self.assertEqual(a.dims,[5])
        self.assertEqual(a.multipliers["0"],1)
        self.assertEqual(a.groupings,{"0":{}})

    def test_full_init(self):
        a = Axe(labels,groupings=groupings)
        self.assertEqual(a.levels,2)
        self.assertEqual(a.dims,[5,3])
        self.assertEqual(len(a),15)
        self.assertEqual(a.multipliers["countries"],3)
        self.assertEqual(a.multipliers["sectors"],1)
        self.assertEqual(a.groupings,groupings)

class Test_Axe_Select(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.a = Axe(labels,groupings=groupings)
        self.fullax = self.a.label()

    def test_basic_select(self):
        sel = self.a[{"countries":"A"}]
        output = [self.fullax[i] for i in sel]
        self.assertEqual(
            output,
                [("A","s1"),("A","s2"),("A","s3")]
                )
        
    def test_sector_select(self):
        sel = self.a["all","s1"]
        output = [self.fullax[i] for i in sel]
        self.assertEqual(
            output,
                [("A","s1"),("B","s1"),("C","s1"),("D","s1"),("E","s1")]
                )
        
    def test_group_select(self):
        sel = self.a["z1",["s1","s2"]]
        output = [self.fullax[i] for i in sel]
        self.assertEqual(
            output,
                [("A","s1"),("A","s2"),("C","s1"),("C","s2")]
                )