import numpy as np
from mrio_toolbox._parts._Part import Part
from mrio_toolbox.mrio import MRIO

labels = {"countries":["a","b","c"],"sectors":["A","B","C"]}

def random_part(dimensions=[["countries"],["sectors"]],
                     name="A"):
    labs = []
    shape = []
    for dim in dimensions:
        lab = dict()
        length = 1
        for d in dim:
            length *= len(labels[d])
            lab[d] = labels[d]
        labs.append(lab)
        shape.append(length)
    shape = tuple(shape)
    return Part(np.random.rand(*shape),name=name,
                labels = labs,
                nature="test")

def random_mrio():
    part_1 = random_part(
        dimensions = [["countries","sectors"],["sectors"]],
        name="rectangle"
    )
    part_square = random_part(
        dimensions = [["countries","sectors"],["countries","sectors"]],
        name="square"
    )
    return MRIO(data={
        "parts":
        {
            "rectangle": part_1,
            "square": part_square
        }
    })