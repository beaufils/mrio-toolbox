"""
Test the initialization of basic objects with the correct attributes
"""

import unittest
from mrio_toolbox._parts._Axe import Axe
from mrio_toolbox._parts._Part import Part
from mrio_toolbox.mrio import MRIO
import numpy as np

class Test_Axe(unittest.TestCase):
    def test_single_level_axe(self):
        l1 = {
            "countries": ["A","B","C","D","E"]
        }
        axe = Axe(l1)
        self.assertEqual(axe.labels,l1)
        self.assertTrue(isinstance(axe,Axe))
        self.assertEqual(axe.dims,[5])

    def test_multiple_axe_level(self):
        l2 = {
            "countries": ["A","B","C","D","E"],
            "sectors": ["s1","s2","s3"]
        }
        axe = Axe(l2)
        self.assertEqual(axe.labels,l2)
        self.assertTrue(isinstance(axe,Axe))
        self.assertEqual(axe.dims,[5,3])


class TestPart(unittest.TestCase):
    def test_basic_part(self):
        """Test initialization with data only"""
        data = np.random.rand(3,3)
        part = Part(data)
        self.assertTrue(isinstance(part,Part))
        np.testing.assert_array_equal(part.data,data)
        self.assertEqual(part.ndim,2)
        self.assertEqual(part.shape,(3,3))
        self.assertEqual(part.name,"new_part")
        self.assertEqual(part.metadata,{})

    def test_empty_part(self):
        """Test initialization from axe only"""
        l1 = {
            "countries": ["A","B","C","D","E"]
        }
        axe = Axe(l1)
        part = Part(axes=[axe])
        self.assertTrue(isinstance(part,Part))
        self.assertEqual(part.ndim,1)
        self.assertEqual(part.shape,(5,))
        self.assertEqual(part.name,"new_part")
        self.assertEqual(part.metadata,{})

    def test_empty_part_with_labels(self):
        """Test initialization from labels only"""
        l1 = {
            "countries": ["A","B","C","D","E"]
        }
        part = Part(labels=l1)
        self.assertTrue(isinstance(part,Part))
        self.assertEqual(part.ndim,1)
        self.assertEqual(part.shape,(5,))
        self.assertEqual(part.name,"new_part")
        self.assertEqual(part.metadata,{})

    def test_invalid_part(self):
        """Test invalid initialization"""
        with self.assertRaises(ValueError):
            Part()

class TestMRIO(unittest.TestCase):
    def test_empty_mrio(self):
        m = MRIO(data={})
        self.assertTrue(isinstance(m,MRIO))

    def test_basic_mrio(self):
        l1 = {
            "countries": ["A","B","C","D","E"]
        }
        part = Part(labels=l1)
        m = MRIO(data={"parts":
                       {
                           "A": part}
                           }
                           )
        self.assertTrue(isinstance(m,MRIO))
        self.assertEqual(m.A,part)

    def test_part_addition_mrio(self):
        m = MRIO(data={})
        l1 = {
            "countries": ["A","B","C","D","E"]
        }
        part = Part(name="A",labels=l1)
        m.add_part(part)
        self.assertEqual(m.A,part)
    
    def test_add_string_dimensions(self):
        m = MRIO()
        labels = {
            # this does not work:
            "countries" : "string"
            # this works: 
            # "countries" : ["string"]
        }
        m.add_dimensions(labels)
        m.parts["A"] = m.new_part(name="A",
            dimensions = ["countries"])
        self.assertEqual(m.A.shape,(1,))
        
    def test_new_part(self):
        m = MRIO(data={})
        dimensions = {
            "countries": ["A","B","C","D","E"],
            "sectors" : ["s1","s2","s3"]
        }
        m.add_dimensions(dimensions)
        m.parts["A"] = m.new_part(name="A",dimensions = ["countries","sectors"])
        self.assertIsInstance(m.A,Part)
        self.assertEqual(m.A.shape,(5,3))