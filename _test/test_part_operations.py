import unittest
import numpy as np
from mrio_toolbox._parts._Part import Part
from test_utils import random_part

class TestReformatting(unittest.TestCase):
    def setUp(self):
        self.part = random_part(
            dimensions = [["countries","sectors"],["sectors"]]
        )
        
    def test_develop_regroup(self):
        developed = self.part.develop()
        regrouped = developed.combine_axes(0,1)
        np.testing.assert_equal(self.part.data,regrouped.data)
        self.assertListEqual(self.part.get_dimensions(),regrouped.get_dimensions())